﻿using UnityEditor;

namespace AssetBundles
{
    public  class BundlesTool
    {
        [MenuItem("Assets/Build AssetsBundle")]
        static void BuildAllAssetBundles()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.Android);
        }
        
    }
}