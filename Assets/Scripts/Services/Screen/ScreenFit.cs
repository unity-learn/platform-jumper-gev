﻿using System.Collections.Generic;
using UnityEngine;

namespace Services.Screen
{
    public class ScreenFit
    {
        
        private static float worldScreenHeight;
        private static float worldScreenWidth;
        private static Vector2 screenSize;
        
        public static float mWorldScreenHeight=>worldScreenHeight;
        public static float mWorldScreenWidth=>worldScreenWidth;
        public static Vector2 mScreenSize => screenSize;
        
        public float AreaSize { get; private set; }
        private  Camera camera { get; set; }
        public ScreenFit(Camera camera)
        {
            this.camera = camera;
            worldScreenHeight = camera.orthographicSize * 2.0f;
            worldScreenWidth = worldScreenHeight / UnityEngine.Screen.height * UnityEngine.Screen.width;
            screenSize=new Vector2(worldScreenWidth,worldScreenHeight);
        }
       /*
        public Dictionary<int, Vector2> GetAreasPoss(float scaleFactor, float offset, int areasCount)
        {
            Dictionary<int, Vector2> AreasPositions;
            float screenWidth = UnityEngine.Screen.width + (1 - scaleFactor) * UnityEngine.Screen.width;
            AreaSize = (screenWidth - (areasCount + 1) * offset) / areasCount;

            AreasPositions = new Dictionary<int, Vector2>();
            Vector2 pos = new Vector2(0, offset + AreaSize / 2);
            for (int i = 0; i < areasCount; i++)
            {
                int k;
                if (i == 0)
                    k = 1;
                else
                    k = 2;

                pos.x = pos.x + offset + k * AreaSize / 2;
                AreasPositions.Add(i, pos);
            }

            return AreasPositions;
        }*/
        
    }
}