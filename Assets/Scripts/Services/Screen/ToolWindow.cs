﻿using System.IO;
using Configs.Audio;
using Configs.Periods;
using Configs.Products;
using Configs.Theme;
using DefaultNamespace;
using Managers;
using UnityEditor;
using UnityEngine;

namespace Services.MenuItems
{
    public class ToolWindow:EditorWindow
    {
        [MenuItem("Tools/Windows/ToolWindow")]
        public static void ShowWindow()
        {
            GetWindow<ToolWindow>("Tools");
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Delete saving"))
            {
                File.Delete(Path.Combine(Application.persistentDataPath, "Saving/game-data.json"));
                Debug.Log("Delete");
            }
        }
        [MenuItem("Tools/Create/AudioManager")]
        public static void CreateAudioManager()
        {
            AudioManager asset = ScriptableObject.CreateInstance<AudioManager>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/Managers/AudioManager.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
        [MenuItem("Tools/Create/Product")]
        public static void CreateProduct()
        {
            ProductConfig asset = ScriptableObject.CreateInstance<ProductConfig>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/Products/Product.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
        [MenuItem("Tools/Create/Period")]
        public static void CreatePeriod()
        {
            PeriodConfig asset = ScriptableObject.CreateInstance<PeriodConfig>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/Periods/Periods.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
        
        [MenuItem("Tools/Create/Effect")]
        public static void CreateEffect()
        {
            EffectConfig asset = ScriptableObject.CreateInstance<EffectConfig>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/Audio/Effects/Effect.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
        [MenuItem("Tools/Create/Sound")]
        public static void CreateSound()
        {
            SoundConfig asset = ScriptableObject.CreateInstance<SoundConfig>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/Audio/Sounds/Sound.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
        [MenuItem("Tools/Create/Theme")]
        public static void CreateTheme()
        {
            ThemeConfig asset = ScriptableObject.CreateInstance<ThemeConfig>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/Themes/Theme.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
    }
}