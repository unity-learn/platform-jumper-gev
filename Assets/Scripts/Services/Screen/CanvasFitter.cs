﻿﻿using System;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class CanvasFitter : MonoBehaviour
    {
        [SerializeField] private RectTransform m_SafeAreaRectTransform;
        private void Awake()
        {
           var safeArea = Screen.safeArea;
            var anchorMin = safeArea.position;
            var anchorMax = anchorMin + safeArea.size;

            anchorMax.x /= Screen.width; 
            anchorMax.y /= Screen.height;
            
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;

            m_SafeAreaRectTransform.anchorMin = anchorMin;
            m_SafeAreaRectTransform.anchorMax = anchorMax;
        }
    }
}