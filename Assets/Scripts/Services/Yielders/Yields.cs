﻿using System.Collections.Generic;
using UnityEngine;

namespace Services.Yielders
{
    public class Yields
    {
        private static Dictionary<float,WaitForSeconds> waits=new Dictionary<float, WaitForSeconds>();
        
        private static WaitForEndOfFrame waitForEndOfFrame=new WaitForEndOfFrame();
        
        private static WaitForFixedUpdate waitForFixedUpdate=new WaitForFixedUpdate();

        public static WaitForEndOfFrame WaitForEndOfFrame => waitForEndOfFrame;
        
        public static WaitForFixedUpdate WaitForFixedUpdate => waitForFixedUpdate;
        
        public static WaitForSeconds GetSecondsDelay(float seconds)
        {
            if (!waits.ContainsKey(seconds))
                waits.Add(seconds,new WaitForSeconds(seconds));
            return waits[seconds];
        }
    }
}