﻿using System.Collections.Generic;
using Core.Game.Platforms;
using Core.Loading;
using UnityEngine;
using UnityEngine.UI;

namespace Configs.Theme
{
    [CreateAssetMenu(fileName = "Themes", menuName = "Themes/theme", order = 0)]
    public class ThemeConfig : ScriptableObject,ILoad
    {
        [SerializeField] private Sprite m_BorderSprite;
        [SerializeField] private List<AbstractPlatform> m_PlatformsList;
        [SerializeField] private Sprite m_BackgroundSprite;
        [SerializeField] private Sprite m_UiPlatformSprite;
        [SerializeField] private Sprite m_PlatformSprite;
        [SerializeField] private string m_Id;
        [SerializeField] private int m_Price;

        public int mPrice => m_Price;

        public Sprite mBorderSprite => m_BorderSprite;

        public Sprite mPlatformSprite => m_PlatformSprite;
        public Sprite mUIPlatformSprite => m_UiPlatformSprite;
        public List<AbstractPlatform> mPlatformsList => m_PlatformsList;

        public Sprite mWorldBackgroundSprite => m_BackgroundSprite;

        public string mId => m_Id;
    }
}