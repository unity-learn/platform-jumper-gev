﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.Audio;

namespace Configs.Audio
{
    [UnityEngine.CreateAssetMenu(fileName = "Sound", menuName = "Audio/Sound", order = 0)]
    public class SoundConfig : ScriptableObject
    {
        [SerializeField] private AudioMixerGroup m_MixerGroup;
        [SerializeField] private AudioClip m_Clip;
        [SerializeField] private SoundType m_SoundType;
        [SerializeField] private float m_Volume;
        [SerializeField] private bool m_Loop;

        
        public SoundType mSoundType => m_SoundType;

        public AudioMixerGroup mAudioMixer => m_MixerGroup;

        public AudioClip mClip => m_Clip;

        public float mVolume => m_Volume;

        public bool mLoop => m_Loop;

    }
}