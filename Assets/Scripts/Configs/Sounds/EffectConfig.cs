﻿using UnityEngine;
using UnityEngine.Audio;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "Effect", menuName = "Audio/Effect", order = 0)]
    public class EffectConfig : ScriptableObject
    {
        [SerializeField] private AudioMixer m_AudioMixer;
        [SerializeField] private AudioClip m_Clip;
        [SerializeField] private float m_Volume;
    }
}