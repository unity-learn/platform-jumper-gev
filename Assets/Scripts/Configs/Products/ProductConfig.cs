﻿using UnityEditor;
using UnityEngine;

namespace Configs.Products
{
    [CreateAssetMenu(fileName = "Product", menuName = "Products/Product", order = 0)]
    public class ProductConfig : ScriptableObject
    {
        public int ProductPrice;
        public string ProductId;
    }
}