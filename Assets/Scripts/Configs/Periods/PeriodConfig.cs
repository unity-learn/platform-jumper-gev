﻿using UnityEditor;
using UnityEngine;

namespace Configs.Periods
{
    [CreateAssetMenu(fileName = "PeriodConfig", menuName = "Periods/periodConfig", order = 0)]
    public class PeriodConfig : ScriptableObject
    {
        [SerializeField] private float m_StaticPlatformsFallTime;
        [SerializeField] private float dynamicPlatformsFallTime;
        [SerializeField] private int m_PlatformsCount;

        [SerializeField] private int m_DynamicPlatformPercent;
        [SerializeField] private int m_SidePlatformPercent;
        
        
        public int DynamicPlatformPercent => m_DynamicPlatformPercent;

        public int SidePlatformPercent => m_SidePlatformPercent;
        public float StaticPlatformsFallTime => m_StaticPlatformsFallTime;

        public float DynamicPlatformsFallTime => dynamicPlatformsFallTime;

        public int PlatformsCount => m_PlatformsCount;
        
    }
}