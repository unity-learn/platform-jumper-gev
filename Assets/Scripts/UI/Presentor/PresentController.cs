﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Platforms;
using Managers;
using Services.Yielders;
using UI.Views;
using UnityEngine;
using Object = System.Object;

namespace UI.Presentor
{
    public  class PresentController:MonoBehaviour
    {
        public List<AbstractPresenter> abstractPresenters { get; private set; }
        private Dictionary<Type, AbstractPresenter> presenterPrefabs;
        private LinkedList<AbstractPresenter> presenters;
        
        private AbstractPresenter activePresenter;

        public AbstractPresenter ActivePresenter => activePresenter;

        private Transform container;
        private UiManager uiManager;
        private int vardan;
        
        public PresentController(UiManager uiManager, List<AbstractPresenter> abstractPresenters, Transform container)
        {
            this.abstractPresenters=new List<AbstractPresenter>();
            presenterPrefabs=new Dictionary<Type, AbstractPresenter>();
            presenters=new LinkedList<AbstractPresenter>();
            
            this.abstractPresenters = abstractPresenters;
            this.container = container;
            this.uiManager = uiManager;
            Setup(this.abstractPresenters);
        }
        private void Setup(List<AbstractPresenter> abstractPresenter)
        {
            for (int i = 0; i < abstractPresenter.Count; i++)
            {
                presenterPrefabs.Add(abstractPresenter[i].GetType(),abstractPresenter[i]);
            }
        }

       public T Show<T>(PresentData data,bool isClose) where T : AbstractPresenter
       {
           Debug.Log(typeof(T));
           if (presenters.Count > 0)
           {
               if (typeof(T).Name==activePresenter.GetType().Name)
               {
                   presenters.LastOrDefault().gameObject.SetActive(true);
                   presenters.LastOrDefault().Show(data);
                   return presenters.LastOrDefault() as T;
               }

               if (isClose)
               {
                   if (activePresenter.Animator!=null)
                       uiManager.StartCoroutine(Animate(activePresenter.Animator,activePresenter));
                   else 
                       activePresenter.gameObject.SetActive(false);
                   
               }

               foreach (var presenter in presenters)
               {
                   if (presenter is T)
                   {
                       presenter.gameObject.SetActive(true);
                       presenter.Show(data);
                       activePresenter = presenter as T;
                       presenters.Remove(presenter);
                       presenters.AddLast(activePresenter);
                       return activePresenter as T;
                   }
               }
               return Create<T>(data);
           }
           return Create<T>(data);
       }

       
       public void Close<T>() where T:AbstractPresenter
       {
           AbstractPresenter closedPresenter = presenterPrefabs[typeof(T)];
           if (closedPresenter.Animator!=null)
               uiManager.StartCoroutine(Animate(closedPresenter.Animator,closedPresenter));
           else 
               closedPresenter.gameObject.SetActive(false);
           
       }
       private IEnumerator Animate(Animator animator,AbstractPresenter presenter)
       {
            presenter.Animator.SetBool("Show", false);
           yield return new WaitForSeconds(animator.GetCurrentAnimatorClipInfo(0).Length);
           presenter.gameObject.SetActive(false);
       }
       private T Create<T>(PresentData data) where T : AbstractPresenter
       {
           activePresenter = Instantiate(presenterPrefabs[typeof(T)], container, false) as T;
           presenterPrefabs[typeof(T)] = activePresenter;
           presenters.AddLast(activePresenter);
           activePresenter.Setup(this,uiManager);
           activePresenter.Show(data);
           return activePresenter as T;

       }
    }
}