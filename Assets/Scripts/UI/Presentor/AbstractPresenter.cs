﻿using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Presentor
{
    public abstract class AbstractPresenter:MonoBehaviour
    {
        [SerializeField] private Animator m_Animator;

        public Animator Animator
        {
            get => m_Animator;
            set => m_Animator = value;
        }

        public abstract void Setup(PresentController controller,UiManager uiManager);
        public abstract void Show(PresentData data);
        public abstract void Close();
    }
}