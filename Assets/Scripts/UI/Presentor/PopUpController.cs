﻿using System.Collections.Generic;
using Managers;
using UnityEngine;

namespace UI.Presentor
{
    public class PopUpController : PresentController
    {
        
        public PopUpController(UiManager uiManager, List<AbstractPresenter> abstractPopUps, Transform container) : base(uiManager, abstractPopUps, container)
        {
        }
    }
}