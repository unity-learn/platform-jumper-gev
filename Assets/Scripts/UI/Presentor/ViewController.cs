﻿using System.Collections.Generic;
using Managers;
using UnityEngine;

namespace UI.Presentor
{
    public class ViewController:PresentController
    {
        public ViewController(UiManager uiManager, List<AbstractPresenter> abstractPresenters, Transform container) : base(uiManager, abstractPresenters, container)
        {
        }
    }
}
       
