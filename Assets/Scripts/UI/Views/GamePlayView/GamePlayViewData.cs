﻿using System;
using Configs.Theme;
using Core.Game;
using Core.Game.Economica;
using Core.Game.Themes;
using Managers;
using UI.Presentor;

namespace UI.Views.ViewData
{
    public class GamePlayViewData:PresentData
    {
        public ScoreManager ScoreManager;
        public EconomyController EconomyController;
        public ThemesController ThemesController;
        public bool InChallenge;
        public int CoinsAmount;
    }
}