﻿using System;
using DefaultNamespace;
using Managers;
using UI.Presentor;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class GamePlayView:AbstractPresenter
    {
        public event Action<bool> OnPause;
        [SerializeField] private Text m_HighScoreText;
        [SerializeField] private Text m_ScoreText;
        [SerializeField] private Button m_PauseButton;

        private GamePlayViewData gamePlayViewData;
        private PresentController controller;
        private UiManager uiManager;

        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            GamePlayViewData data= presentData as GamePlayViewData;
            if (data!=null)
            {
                if (data.InChallenge)
                    m_PauseButton.gameObject.SetActive(false);
                else
                    m_PauseButton.gameObject.SetActive(true);

                gamePlayViewData = data;
                gamePlayViewData.ScoreManager.OnScoreUpdate += UpdateScore;
                m_PauseButton.onClick.RemoveAllListeners();
                m_PauseButton.onClick.AddListener(Pause);

            }
            
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }
        
        private void UpdateScore(int highScore, int currentScore)
        {
            m_HighScoreText.text ="High Score:  "+ highScore;
            m_ScoreText.text = "Score:  "+currentScore;
        }

        private void Pause()
        {
            OnPause?.Invoke(true);
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);

        }
        public override void Close()
        {
            controller.Close<GamePlayView>();
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);

            if (gamePlayViewData==null)
            {
                return;
            }
            gamePlayViewData.ScoreManager.OnScoreUpdate -= UpdateScore;

        }
    }
}