﻿using System;
using DefaultNamespace;
using Managers;
using TMPro;
using UI.PopUp;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class MainView : AbstractPresenter
    {
        public event Action OnGamePlayStart;
        public event Action OnDailyChallenge;
        [SerializeField] private Button m_PlayButton;
        [SerializeField] private Button m_DailyChallengeButton;
        [SerializeField] private Button m_SettingsButton;
        [SerializeField] private Button m_StoreButton;
        [SerializeField] private Button m_RecordsButton;
        [SerializeField] private Button m_Boost;
        [SerializeField] private Text m_CoinsAmountText;
        [SerializeField] private TextMeshProUGUI m_ChallengeTimeText;
        [SerializeField] private Image m_PlatformImage;

        public TextMeshProUGUI ChallengeTimeText
        {
            get => m_ChallengeTimeText;
            set => m_ChallengeTimeText = value;
        }


        public Image mPlatformImage
        {
            get => m_PlatformImage;
            set => m_PlatformImage = value;
        }

        private MainViewData mainViewData;
        private PresentController controller;
        private UiManager uiManager;
        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            MainViewData data= presentData as MainViewData;
            if (data!=null)
            {
                mainViewData = data;
                mainViewData.EconomyController.OnCoinsUpdate += UpdateCoins;
                m_CoinsAmountText.text = mainViewData.coinsAmount.ToString();
                m_PlatformImage.sprite = mainViewData.ThemesController.mCurrentTheme.mUIPlatformSprite;
                
                m_PlayButton.onClick.RemoveAllListeners();
                m_DailyChallengeButton.onClick.RemoveAllListeners();
                m_SettingsButton.onClick.RemoveAllListeners();
                m_StoreButton.onClick.RemoveAllListeners();
                m_RecordsButton.onClick.RemoveAllListeners();
                m_Boost.onClick.RemoveAllListeners();
                
                m_PlayButton.onClick.AddListener(OnPlay);
                m_DailyChallengeButton.onClick.AddListener(OnChallenge);
                m_SettingsButton.onClick.AddListener(OnSettings);
                m_StoreButton.onClick.AddListener(OnStore);
                m_RecordsButton.onClick.AddListener(OnRecords);
                m_Boost.onClick.AddListener(OnBoost);
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }
        private void UpdateCoins(int coinsAmount)
        { 
            m_CoinsAmountText.text = coinsAmount.ToString();
            mainViewData.coinsAmount = coinsAmount;

        }
        private void OnPlay()
        {
            OnGamePlayStart?.Invoke();
            Close();
        }

        private void OnChallenge()
        {
            Debug.Log("gago");
            OnDailyChallenge?.Invoke();
        }
        private void OnStore()
        {
            controller.Show<StoreView>(new StoreViewData()
            {
                CoinsAmount = mainViewData.coinsAmount,
                EconomyController =mainViewData.EconomyController,
                ThemesController =mainViewData.ThemesController
            }, true);
            Close();
        }
        private void OnSettings()
        {
            uiManager.PopUpController.Show<SettingsPopUp>(new SettingsPopUpData(mainViewData.coinsAmount), false);
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);

        }

        private void OnBoost()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            uiManager.PopUpController.Show<BoostPopUp>(new BoostPopUpData(), false);

        }

        private void OnRecords()
        {
            uiManager.PopUpController.Show<RecordsPopUp>(new RecordPopUpData(), false);
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);

        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            mainViewData.EconomyController.OnCoinsUpdate -= UpdateCoins;
            uiManager.ViewController.Close<MainView>();
        }
    }
}