﻿using System;
using Core.Game.Economica;
using Core.Game.Themes;
using Managers;
using UI.Presentor;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views.ViewData
{
    public class MainViewData:PresentData
    {
        public Action OnPlay;
        public UiManager UiManager;
        public EconomyController EconomyController;
        public ThemesController ThemesController;
        public int coinsAmount;
        public int currentScore;
        public int highScore;
        
       
    }
}