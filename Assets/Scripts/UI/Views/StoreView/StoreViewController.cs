﻿using System;
using System.Collections;
using System.Collections.Generic;
using Configs.Theme;
using Services.Yielders;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class StoreViewController : MonoBehaviour
    {
        [SerializeField] private RectTransform m_CircleRectTransform;
        [SerializeField] private Image m_Cirecle;
        
        public List<AbstractItem> items=new List<AbstractItem>();
        private Dictionary<int, Vector3> itemPoss;
        private Quaternion cirecleRectTransform;
        private int rotateItemsCount;
//In process
        private void Awake()
        {
            Setup();
        }
        private void Setup()
        {
            rotateItemsCount = items.Count;
            itemPoss=new Dictionary<int, Vector3>();
            for (int i = 0; i <  items.Count; i++) 
            {
                float angle = (i) * Mathf.PI*2/  (rotateItemsCount-1);
                Vector2 pos = new Vector2(Mathf.Cos(angle),Mathf.Sin(angle))* m_CircleRectTransform.rect.width/2;
                
                itemPoss.Add(i,pos);
                
                items[i].transform.localPosition = pos;
                items[i].mPosIndex = i;
                items[i].mCurrentPos = pos;
            }
        }
        
        private void MoveNextRight()
        {
            for (int i = 0; i <  items.Count; i++)
            {
                AbstractItem item = items[i];
                if (itemPoss.Count<=i+1)
                {
                    items[0] = item;
                    items[0].mPosIndex = 0;
                    items[0].mCurrentPos = itemPoss[0];
                }
                items[i + 1] = item;
                items[i + 1].mPosIndex = i + 1;
                items[i + 1].mCurrentPos = itemPoss[i + 1];
                
                /* float angle = angles[i]+Mathf.PI/products.Count;
                 products[i].transform.SetSiblingIndex(products.Count-1);
                 angles[i] = angle;
                 Vector3 pos = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * m_CircleRectTransform.rect.width /2;
                 products[i].transform.localPosition=pos;
                */
            }
        }

        private void MoveNextLeft()
        {
            for (int i = 1; i <= items.Count; i++)
            {
                AbstractItem item = items[i];
                if (itemPoss.Count<=i+1)
                {
                    items[0] = item;
                    items[0].mPosIndex = 0;
                    items[0].mCurrentPos = itemPoss[0];
                }
                items[i-1] = item;
                items[i-1].mPosIndex = i-1;
                items[i-1].mCurrentPos = itemPoss[i-1];
                
                
            }
        }



    }
}