﻿using System;
using Core.Game.Economica;
using Core.Game.Themes;
using UI.Presentor;

namespace UI.Views.ViewData
{
    public class StoreViewData:PresentData
    {
        public event Action OnMainView;
        public EconomyController EconomyController;
        public ThemesController ThemesController;
        public int CoinsAmount;
       
    }
}