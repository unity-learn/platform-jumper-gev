﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Game.Economica;
using DefaultNamespace;
using Managers;
using UI.PopUp;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Views
{
    public class StoreView :AbstractPresenter
    {
        public event Action<string> OnBuy;

        [SerializeField] private Button m_BoostButton;
        [SerializeField] private Button m_BackButton;
        [SerializeField] private Text m_CoinsAmount;
        [SerializeField] private Sprite m_SetButtonSprite;
        [SerializeField] private Sprite m_BuyButtonSprite;
        
        public List<Button> ThemesButtons=new List<Button>();
        private Dictionary<int, Button> Themes;
        private StoreViewData storeViewData;
        private PresentController controller;
        private UiManager uiManager;

        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
            Themes=new Dictionary<int, Button>();
            
            
            for (int i = 0; i < ThemesButtons.Count; i++)
            {
                Themes.Add(i+1,ThemesButtons[i]);
            }
        }

        public override void Show(PresentData presentData)
        {
            StoreViewData data= presentData as StoreViewData;
            if (data!=null)
            {
                storeViewData = data;
                SortThemes();
                storeViewData.EconomyController.OnCoinsUpdate += UpdateCoins;
                storeViewData.ThemesController.OnUpdateTheme += SortThemes;
                m_CoinsAmount.text = storeViewData.CoinsAmount.ToString();
                m_BoostButton.onClick.RemoveAllListeners();
                m_BackButton.onClick.RemoveAllListeners();
                
                m_BackButton.onClick.AddListener(OnMainView);
                m_BoostButton.onClick.AddListener(Boost);
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }

        private void SortThemes()
        {
            for (int i = 1; i <= Themes.Keys.Count; i++)
            {
                string themeId = i.ToString();
                Themes[i].onClick.RemoveAllListeners();
                Themes[i].onClick.AddListener(()=>uiManager.GameManager.ThemesController.UnLockCheck(themeId));
                if (!uiManager.GameManager.SaveManager.SaveData.UnLockedThemes.Contains(themeId))
                    Themes[i].image.sprite = m_BuyButtonSprite;


                else
                {
                    Themes[i].image.sprite = m_SetButtonSprite;
                }
            }
        }
        private void UpdateCoins(int coinsAmount)
        { 
            m_CoinsAmount.text = coinsAmount.ToString();
            storeViewData.CoinsAmount = coinsAmount; 
        }
        
        private void OnMainView()
        {
            controller.Show<MainView>(new MainViewData()
            {
                coinsAmount = storeViewData.CoinsAmount,
                EconomyController = storeViewData.EconomyController,
                ThemesController = storeViewData.ThemesController,
            }, false);
            
            Close();
        }
        
        private void Boost()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            uiManager.PopUpController.Show<BoostPopUp>(new BoostPopUpData(), false);
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick); 
            storeViewData.EconomyController.OnCoinsUpdate -= UpdateCoins;
            storeViewData.ThemesController.OnUpdateTheme -= SortThemes;

            controller.Close<StoreView>();
        }
    }
}