using UnityEngine;

namespace DefaultNamespace
{
    public abstract class AbstractItem : MonoBehaviour
    {
        [SerializeField] protected  int m_PosIndex;

        public int mPosIndex
        {
            get => m_PosIndex;
            set => m_PosIndex = value;
        }

        protected Vector3 currentPos;

        public Vector3 mCurrentPos
        {
            get => currentPos;
            set => currentPos = value;
        }
    }
}