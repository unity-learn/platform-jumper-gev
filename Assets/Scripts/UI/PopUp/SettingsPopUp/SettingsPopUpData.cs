﻿using System;
using UI.Presentor;

namespace UI.Views.ViewData
{
    public class SettingsPopUpData:PresentData
    {
        public event Action OnMainView;
        public event Action OnSave;

        public int coinsAmount;
        public SettingsPopUpData(int coinsAmount)
        {
            this.coinsAmount = coinsAmount;
        }
    }
}