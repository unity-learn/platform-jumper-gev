﻿using System;
using DefaultNamespace;
using Managers;
using UI.PopUp;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;
using AudioType = Configs.Audio.AudioType;

namespace UI.Views
{
    public class SettingsPopUp :AbstractPresenter
    {
        public event Action OnGamePlayStart;
        [SerializeField] private Button m_BoostButton;
        [SerializeField] private Button m_BackButton;
        [SerializeField] private Text m_CoinsAmount;
        [SerializeField] private Button m_MusicButton;
        [SerializeField] private Button m_EffectsButton;
        [SerializeField] private Sprite m_MuteSprite;
        [SerializeField] private Sprite m_UnMuteSprite;

        private SettingsPopUpData m_SettingsPopUpData;
        private PresentController controller;
        private UiManager uiManager;

        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            SettingsPopUpData data= presentData as SettingsPopUpData;
            if (data!=null)
            {
                Animator.SetBool("Show", true);
                if (uiManager.GameManager.SaveManager.SaveData.IsMusicMute)
                    m_MusicButton.image.sprite =m_MuteSprite;
                if (uiManager.GameManager.SaveManager.SaveData.IsEffectsMute)
                    m_EffectsButton.image.sprite =m_MuteSprite;

                m_SettingsPopUpData = data;
                m_CoinsAmount.text = m_SettingsPopUpData.coinsAmount.ToString();
                
                m_BoostButton.onClick.RemoveAllListeners();
                m_BackButton.onClick.RemoveAllListeners();
                m_MusicButton.onClick.RemoveAllListeners();
                m_EffectsButton.onClick.RemoveAllListeners();
                
                
                m_BackButton.onClick.AddListener(OnMainView);
                m_BoostButton.onClick.AddListener(Boost);
                m_MusicButton.onClick.AddListener(MuteMusic);
                m_EffectsButton.onClick.AddListener(MuteEffects);
              
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }

        private void MuteEffects()
        {
            uiManager.GameManager.SaveManager.SaveData.IsEffectsMute =
                !uiManager.GameManager.SaveManager.SaveData.IsEffectsMute;
            uiManager.GameManager.mAudioManager.Mute(AudioType.Effect,uiManager.GameManager.SaveManager.SaveData.IsEffectsMute);
            if (uiManager.GameManager.SaveManager.SaveData.IsEffectsMute)
                m_EffectsButton.image.sprite =m_MuteSprite;
            else
                m_EffectsButton.image.sprite =m_UnMuteSprite;

        }

        private void MuteMusic()
        {
            uiManager.GameManager.SaveManager.SaveData.IsMusicMute =
                !uiManager.GameManager.SaveManager.SaveData.IsMusicMute;
            uiManager.GameManager.mAudioManager.Mute(AudioType.Music,uiManager.GameManager.SaveManager.SaveData.IsMusicMute);
            if (uiManager.GameManager.SaveManager.SaveData.IsMusicMute)
                m_MusicButton.image.sprite =m_MuteSprite;
            else
                m_MusicButton.image.sprite =m_UnMuteSprite;
        }

        private void OnMainView()
        {

            Close();
        }
        private void Boost()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            uiManager.PopUpController.Show<BoostPopUp>(new BoostPopUpData(), true);
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            controller.Close<SettingsPopUp>();
        }
    }
}