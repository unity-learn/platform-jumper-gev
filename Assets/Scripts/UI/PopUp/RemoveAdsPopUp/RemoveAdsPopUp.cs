﻿using DefaultNamespace;
using Managers;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUp
{
    public class RemoveAdsPopUp:AbstractPresenter
    {
        [SerializeField] private Button m_AcceptButton;
        [SerializeField] private Button m_DenyButton;
        private RemoveAdsPopUpData removeAdsPopUpData;
        private PresentController controller;
        private UiManager uiManager;


        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            ShearPopUpData data=presentData as ShearPopUpData;
            if (data!=null)
            {
                Animator.SetBool("Show", true);
                m_AcceptButton.onClick.RemoveAllListeners();
                m_DenyButton.onClick.RemoveAllListeners();
                
                m_AcceptButton.onClick.AddListener(Accept);
                m_DenyButton.onClick.AddListener(Deny);
            }
        }
        private void Accept()
        {
            Close();
        }

        private void Deny()
        {
            Close();
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            controller.Close<RemoveAdsPopUp>();
        }

    }
}