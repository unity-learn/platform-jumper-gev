﻿using System;
using System.Collections.Generic;
using DefaultNamespace;
using Managers;
using UI.PopUp;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Views
{
    public class RecordsPopUp:AbstractPresenter
    {
        [SerializeField] private Button m_BoostButton;
        [SerializeField] private Button m_BackButton;
        [SerializeField] private Text m_CoinsAmount;
        public List<Text> scores=new List<Text>();

        private RecordPopUpData recordsPopUpData;
        private PresentController controller;
        private UiManager uiManager;

        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            RecordPopUpData data= presentData as RecordPopUpData;
            if (data!=null)
            {
                Animator.SetBool("Show", true);
                recordsPopUpData = data;
                if (scores.Count > 0 &&  this.uiManager.GameManager.SaveManager.SaveData.ScoreDashboard!=null && this.uiManager.GameManager.SaveManager.SaveData.ScoreDashboard.Count>0)
                {
                    int scoreDashboardCount = uiManager.GameManager.SaveManager.SaveData.ScoreDashboard.Count;
                    for (int i = 0; i <scoreDashboardCount ; i++)
                    {
                        scores[i].text = (scoreDashboardCount-i) + "th           " +
                                         this.uiManager.GameManager.SaveManager.SaveData.ScoreDashboard[i];
                    }
                   
                }
                else
                    scores[1].text = " No records ";//Middle text bar
                
               
                m_BoostButton.onClick.RemoveAllListeners();
                m_BackButton.onClick.RemoveAllListeners();
                
                m_BackButton.onClick.AddListener(Close);
                m_BoostButton.onClick.AddListener(Boost);
              
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }
        
        private void Boost()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            uiManager.PopUpController.Show<BoostPopUp>(new BoostPopUpData(), true);
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);

            controller.Close<RecordsPopUp>();
        }
    }
}