﻿using Core.Game.Ads;
using DefaultNamespace;
using Managers;
using UI.PopUp.ConnectionErrorPopUp;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUp
{
    public class BoostPopUp:AbstractPresenter
    {
        [SerializeField] private Button m_AcceptButton;
        [SerializeField] private Button m_DenyButton;
        private BoostPopUpData boostPopUpData;
        private PresentController controller;
        private UiManager uiManager;

        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            BoostPopUpData data=presentData as BoostPopUpData;
            if (data!=null)
            {
                Animator.SetBool("Show", true);
                boostPopUpData = data;
                m_AcceptButton.onClick.RemoveAllListeners();
                m_DenyButton.onClick.RemoveAllListeners();
                
                m_AcceptButton.onClick.AddListener(Accept);
                m_DenyButton.onClick.AddListener(Deny);
            }
        }
        private void Accept()
        {
            if (!uiManager.GameManager.AdsManager.Show(AdType.RewardVideo))
                controller.Show<ConnectionErrorPopUp.ConnectionErrorPopUp>(new ConnectionErrorPopUpData(), true);
            
        }

        private void Deny()
        {
            Close();
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            controller.Close<BoostPopUp>();
        }

    }
}