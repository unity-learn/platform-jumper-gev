﻿using Configs.Theme;
using UI.Presentor;

namespace UI.PopUp.BuyPopUp
{
    public class BuyPopUpData:PresentData
    {
        public ThemeConfig ThemeConfig;
        public int coinsAmount;
        public string takenThemeId;
    }
}