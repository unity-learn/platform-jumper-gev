﻿using DefaultNamespace;
using Managers;
using UI.Presentor;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUp.BuyPopUp
{
    public class BuyPopUp:AbstractPresenter
    {
        [SerializeField] private Button m_BuyButton;
        [SerializeField] private Button m_DenyButton;
        [SerializeField] private Text m_CoinsAmount;

        private BuyPopUpData buyPopUpData;
        private PresentController controller;
        private UiManager uiManager;
        public override void Setup(PresentController controller, UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            BuyPopUpData data= presentData as BuyPopUpData;
            if (data!=null)
            { 
                Animator.SetBool("Show", true);
                buyPopUpData = data;
               m_BuyButton.onClick.RemoveAllListeners();
               m_DenyButton.onClick.RemoveAllListeners();
               
               m_BuyButton.onClick.AddListener(Buy);
               m_DenyButton.onClick.AddListener(Close);
               
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }

        private void Buy()
        { 
            uiManager.GameManager.ThemesController.LoadTheme(buyPopUpData.takenThemeId);
            Close();
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            controller.Close<BuyPopUp>();
        }
    }
}