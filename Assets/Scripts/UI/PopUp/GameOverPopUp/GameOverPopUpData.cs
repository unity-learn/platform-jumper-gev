﻿using System;
using Core.Game.Economica;
using Core.Game.Themes;
using UI.Presentor;

namespace UI.Views.ViewData
{
    public class GameOverPopUpData:PresentData
    {
        public  Action OnMainView;
        public  Action OnReplay;
        public EconomyController EconomyController;
        public ThemesController ThemesController;
        public bool InChallenge;
        public int coinsAmount;
        public int currentCoins;
        public int currentScore;
        public int highScore;

        
    }
}