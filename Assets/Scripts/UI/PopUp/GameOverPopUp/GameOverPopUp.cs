﻿using System;
using DefaultNamespace;
using Managers;
using UI.Presentor;
using UI.Views;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUp
{
    public class GameOverPopUp : AbstractPresenter
    {
        public event Action OnReplay;
        public event Action OnLeftGamePlay;
        [SerializeField] private Button m_ReplayButton;
        [SerializeField] private Button m_MainViewButton;
        [SerializeField] private Text m_CoinsAmount;
        [SerializeField] private Text m_EarnedCoinsAmount;

        private GameOverPopUpData gameOverPopUpData;
        private PresentController controller;
        private UiManager uiManager;
        public override void Setup(PresentController controller,UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }
        public override void Show(PresentData presentData)
        {
            GameOverPopUpData data= presentData as GameOverPopUpData;
            if (data!=null)
            {
                if (data.InChallenge)
                    m_ReplayButton.gameObject.SetActive(false);
                else
                    m_ReplayButton.gameObject.SetActive(true);

                Animator.SetBool("Show", true);
                gameOverPopUpData = data;
                gameOverPopUpData.OnReplay +=Replay;
                m_CoinsAmount.text = gameOverPopUpData.coinsAmount.ToString();
                m_EarnedCoinsAmount.text = "<b>Please try again, focus! \n</b> "+"Time "+uiManager.GameManager.SaveManager.SaveData.GamePlayTime +"\n<b> Earned coins  </b>" + gameOverPopUpData.currentCoins;
                
                m_ReplayButton.onClick.RemoveAllListeners();
                m_ReplayButton.onClick.AddListener(gameOverPopUpData.OnReplay.Invoke);

                m_MainViewButton.onClick.RemoveAllListeners();
                m_MainViewButton.onClick.AddListener(OnMainView);
               
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }

        private void OnMainView()
        {
            OnLeftGamePlay?.Invoke();
            uiManager.ViewController.Show<MainView>(new MainViewData()
            {
                coinsAmount = gameOverPopUpData.coinsAmount,
                EconomyController = gameOverPopUpData.EconomyController,
                ThemesController = gameOverPopUpData.ThemesController
            }, true);
            Close();
            
        }
        private void Replay()
        {
            gameOverPopUpData.OnReplay -=Replay;
            OnReplay?.Invoke();
            Close();
        }
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            controller.Close<GameOverPopUp>();
        }
    }
}