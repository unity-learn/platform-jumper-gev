﻿using System;
using Managers;
using UI.Presentor;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.UI.PopUp.PausePopUp
{
    public class PausePopUp:AbstractPresenter
    {
        public event Action<bool> OnResume;
        public event Action OnRestart;
        public event Action OnQuit;
        [SerializeField] private Button m_ResumeButton;
        [SerializeField] private Button m_RestartButton;
        [SerializeField] private Button m_QuitButton;

        private PausePopUpData pausePopUpData;
        private PresentController controller;
        private UiManager uiManager;
        public override void Setup(PresentController controller, UiManager uiManager)
        {
            this.controller = controller;
            this.uiManager = uiManager;
        }

        public override void Show(PresentData presentData)
        {
            PausePopUpData data= presentData as PausePopUpData;
            if (data!=null)
            {
                Animator.SetBool("Show", true);
                pausePopUpData = data;
                m_RestartButton.onClick.RemoveAllListeners();
                m_QuitButton.onClick.RemoveAllListeners();
                m_ResumeButton.onClick.RemoveAllListeners();
                
                m_RestartButton.onClick.AddListener((() => OnRestart?.Invoke()));
                m_QuitButton.onClick.AddListener((() => OnQuit?.Invoke()));
                m_ResumeButton.onClick.AddListener((() => OnResume?.Invoke(false)));
               
            }
            else
            {
                Debug.LogError(presentData+" data is null");
            }
        }
        
        
        public override void Close()
        {
            uiManager.GameManager.mAudioManager.Play(SoundType.ButtonClick);
            controller.Close<PausePopUp>();
        }
    }
}