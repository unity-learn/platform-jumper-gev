﻿using Managers;
using UI.Presentor;
using UnityEngine;
using UnityEngine.UI;

namespace UI.PopUp.ConnectionErrorPopUp
{
    public class ConnectionErrorPopUp:AbstractPresenter
    {
        [SerializeField] private Button m_Accept;

        private ConnectionErrorPopUpData connectionErrorPopUpData;
        private PresentController controller;
        private UiManager uiManager;
        public override void Setup(PresentController controller, UiManager uiManager)
        {
            this.uiManager = uiManager;
            this.controller = controller;
        }

        public override void Show(PresentData presentData)
        {
            ConnectionErrorPopUpData data =presentData as ConnectionErrorPopUpData;
            if (data!=null)
            {
                
                connectionErrorPopUpData = data;
                Animator.SetBool("Show", true);

                m_Accept.onClick.RemoveAllListeners();
                
                m_Accept.onClick.AddListener(Close);
            }
        }

        public override void Close()
        {
            controller.Close<ConnectionErrorPopUp>();
        }
    }
}