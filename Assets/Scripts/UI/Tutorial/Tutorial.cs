﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Tutorial
{
    public class Tutorial : MonoBehaviour,IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            Destroy(this.gameObject);
        }
    }
}