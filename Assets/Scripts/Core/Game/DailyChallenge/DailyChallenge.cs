﻿using System;
using System.Collections;
using Configs.Periods;
using Services.Yielders;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

namespace Core.Game.DailyChallenge
{
    public class DailyChallenge 
    {
        public bool ChallengeIsActve { get; private set; }
        
        private PeriodConfig periodConfig;
        private MonoBehaviour rutine;
        private DateTime previousTime;
        private TextMeshProUGUI timeText;
        private int waitTimeInSeconds;
        private int timeInSeconds;
        private bool inDelay;
        public DailyChallenge(PeriodConfig periodConfig, MonoBehaviour rutine, TextMeshProUGUI timeText)
        {
            this.periodConfig = periodConfig;
            this.rutine = rutine;
            this.timeText = timeText;
            waitTimeInSeconds = 86400;//one day seconds amount
        }
        public void Setup(DateTime previousTime)
        {
            inDelay = true;
            this.previousTime = previousTime;
            rutine.StartCoroutine(Start());
        }

        private IEnumerator Start()
        {
            ChallengeIsActve = false;
            timeInSeconds = 0;
            while (waitTimeInSeconds>=timeInSeconds && previousTime!=null)
            {
                timeInSeconds =(int) (DateTime.Now - previousTime).TotalSeconds;
                timeText.text = GetDelayTime(waitTimeInSeconds - timeInSeconds);
                yield return Yields.GetSecondsDelay(1);
            }
            Debug.Log("daily challenge is available");
            timeText.text = "Available";
            ChallengeIsActve = true;
        }
        private string GetDelayTime(int dTimeToSec)
        {
            string stringSec = "";
            string stringMin = "";
            int hours = dTimeToSec / 3600;
            int sec = dTimeToSec % 60;
            int min = (dTimeToSec-(hours*3600)-sec) / 60;
            if (sec < 10)
                stringSec = "0" + sec;
            else
                stringSec = sec.ToString();
            if (min < 10)
                stringMin = "0" + min;
            else
                stringMin = min.ToString();
            

            return hours + ":" + stringMin + ":" + stringSec;
        }
    }
}
