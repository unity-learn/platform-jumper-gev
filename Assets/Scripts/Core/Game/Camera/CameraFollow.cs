﻿using System;
using UnityEngine;

namespace Core.Game.Camera
{
    public class CameraFollow : MonoBehaviour,IStartGamePlay
    { 
        [SerializeField] private UnityEngine.Camera m_Camera;
        [SerializeField] private float m_FollowSpeed=5;
        
        private bool camIsMoved;
        private bool gamePlayStart;
        private Rigidbody2D followedBody;

        public bool mCamIsMoved
        {
            get => camIsMoved;
            set => camIsMoved = value;
        }
        public bool mCameraCanMove
        {
            get => cameraCanMove;
            set => cameraCanMove = value;
        }
        private Vector3 camStartMovePos = new Vector3(0.5f, .5f, -10);
        private Vector3 cameraPos;
        private bool cameraCanMove;


        public void Setup(Rigidbody2D followedBody)
        {
            this.followedBody = followedBody;
        }
        public void StartGamePlay()
        {
           
            cameraCanMove = true;
            mCameraCanMove = true;
            mCamIsMoved = false;
            gamePlayStart = true;
            transform.position =new Vector3(0,0,-10);
            cameraPos = transform.position;
        }
        
        private void FixedUpdate()
        {
            if (gamePlayStart)
            {
                if (mCamIsMoved && mCameraCanMove)
                {
                    cameraPos.y = Mathf.Lerp(cameraPos.y, followedBody.position.y, Time.deltaTime * m_FollowSpeed);
                    transform.position = cameraPos;
                    return;
                }
                if (m_Camera.ViewportToWorldPoint(camStartMovePos).y <= followedBody.transform.position.y)
                {
                    mCamIsMoved = true;
                }
            }
        }


    }
}