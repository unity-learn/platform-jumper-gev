﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Configs.Periods;
using Configs.Theme;
using Core.Game.Border;
using Core.Game.Camera;
using Core.Game.Platforms;
using Core.Game.Player;
using Core.Mechanic.DeathZone;
using Core.Mechanic.Factory;
using Core.Mechanic.Generator;
using DefaultNamespace;
using DefaultNamespace.UI.PopUp.PausePopUp;
using Managers;
using Services.Screen;
using Services.Yielders;
using UI.PopUp;
using UI.Views;
using UI.Views.ViewData;
using UnityEngine;

namespace Core.Game
{
    public class GamePlayController : MonoBehaviour,IClean,IReplay,IGameOver
    {
        
        [SerializeField] private PlayerController m_PlayerControllerPrefab;
        [SerializeField] private GameManager m_GameManager;
        [SerializeField] private SaveManager m_SaveManager;
        [SerializeField] private Bordercontroller m_BorderController;
        [SerializeField] private PlatformGenerator m_PlatformGenerator;
        [SerializeField] private PlatformsFactory m_PlatformsFactory;
        [SerializeField] private PoolManager m_PoolManager;
        [SerializeField] private InputManager m_InputManager;
        [SerializeField] private ScoreManager m_ScoreManager;
        [SerializeField] private UnityEngine.Camera m_Camera;
        [SerializeField] private SpriteRenderer m_WorldBackground;
        [SerializeField] private CameraFollow m_CameraFollow;
        [SerializeField] private DeathZone m_DeathZone;
        [SerializeField] private PeriodConfig m_DailChallengePeriodConfig;
        [SerializeField] private float m_DeathPosOffset=3;
        [SerializeField] private float m_PlayerStartPosYOffset;
        [SerializeField] private int m_RecordsCount=3;
        
        public List<PeriodConfig> periods=new List<PeriodConfig>();

        public GameManager mGameManager => m_GameManager;
        private List<int> scoreDashboard;
        private ThemeConfig themeConfig;
        private PeriodConfig currentPeriodConfig;
        private PeriodConfig defaultPeriodConfig;
        private GameOverPopUp gameOverPopUp;
        private PausePopUp pausePopUp;
        private GamePlayView gamePlayView;
        private AbstractPlatform landedPlatform;
        private DateTime dataTime;
        private DateTime startGamePlayTime;
        private TimeSpan gamePlayTime;
        private Vector2 playerPos;
        
        //private float worldScreenHeight;
        //private float worldScreenWidth;
        private bool isGamePlayStart;
        private bool inGamePlay;
        private bool inChallenge;
        private int gameId;
        private bool inPeriod;
        private bool isLastPeriod;
        
        public PlayerController mPlayerController { get; private set; }
        public PlatformGenerator mPlatformGenerator => m_PlatformGenerator;
        public void Setup(ThemeConfig themeConfig)
        {
            this.themeConfig = themeConfig;
            m_WorldBackground.sprite=this.themeConfig.mWorldBackgroundSprite;
            scoreDashboard =m_SaveManager.SaveData.ScoreDashboard;
            
            //worldScreenHeight = m_Camera.orthographicSize * 2.0f;
            //worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
            
            if (mPlayerController==null)
            {
                mPlayerController = Instantiate(m_PlayerControllerPrefab, transform); 
                mPlayerController.gameObject.SetActive(false);
                playerPos = new Vector2(ScreenFit.mWorldScreenWidth/2 - m_GameManager.ThemesController.mCurrentTheme.mPlatformSprite.bounds.size.x/6 ,
                    m_PlayerStartPosYOffset+m_GameManager.ThemesController.mCurrentTheme.mPlatformSprite.bounds.size.y-ScreenFit.mWorldScreenHeight/2);
                mPlayerController.Setup(m_Camera,m_InputManager,this);
            }
            m_BorderController.Setup(this.themeConfig.mBorderSprite);
            m_PlatformsFactory.Setup(this.themeConfig.mPlatformsList);
            m_PlatformGenerator.Setup();
            m_CameraFollow.Setup(mPlayerController.mRigidbody);
        }
        public void StartGamePlay(bool inChallenge=false)
        {
            inPeriod = true;
            this.inChallenge = inChallenge;
            m_PlatformGenerator.CurrentPeriodConfig =periods.FirstOrDefault();
            if (inChallenge)
            {
                m_PlatformGenerator.CurrentPeriodConfig =m_DailChallengePeriodConfig;
                StartCoroutine(PeriodRunner(m_DailChallengePeriodConfig, inChallenge));
            }
            else
                StartCoroutine(PeriodRunner(periods.FirstOrDefault(),inChallenge));

            gamePlayView= m_GameManager.mUiManager.ViewController.Show<GamePlayView>(new GamePlayViewData()
            {
                ScoreManager =m_ScoreManager,
                CoinsAmount = m_SaveManager.SaveData.CoinsAmount,
                EconomyController = m_GameManager.EconomyController,
                ThemesController = m_GameManager.ThemesController,
                InChallenge = this.inChallenge
            },true);

            if (gameOverPopUp != null)
            { 
                gameOverPopUp.OnReplay -= Replay;
                gameOverPopUp.OnLeftGamePlay -= CleanUp;
                gameOverPopUp.Close();
            }

            isGamePlayStart = true;
            inGamePlay = true; 
            startGamePlayTime = DateTime.Now;
            m_GameManager.SaveManager.SaveData.GameId += 1;
            
            mPlayerController.transform.position = playerPos;
            FitBeckground(m_Camera,m_WorldBackground);
            
            m_CameraFollow.StartGamePlay();
            m_ScoreManager.StartGamePlay();
            m_BorderController.StartGamePlay();
            m_PlatformGenerator.StartGamePlay();
            mPlayerController.StartGamePlay();

            mPlayerController.OnPlatformLended += PlatformLanded;
            mPlayerController.OnPlatformFall +=PlatformFall;
            m_DeathZone.OnPlayerDeath += GameOver;
            m_DeathZone.OnAddPool += ConsumePlatform;
            gamePlayView.OnPause += PauseGamePlay;

            m_InputManager.enabled=true;
            m_BorderController.gameObject.SetActive(true);
            mPlayerController.gameObject.SetActive(true);
            m_WorldBackground.gameObject.SetActive(true);
        }
        
        private void PlatformLanded(AbstractPlatform platform)
        {
            landedPlatform = platform;
            platform.mPlatformFall = true;
            m_ScoreManager.UpdateScore(platform.PlatformScore);
        }

        private void PlatformFall(Collision2D platform)
        {
            AbstractPlatform fallPlatform = platform.gameObject.GetComponent<AbstractPlatform>();
            if (fallPlatform!=null)
            {
                fallPlatform.IsFirst = false;
                fallPlatform.mRb.isKinematic = false;
                fallPlatform.mRb.gravityScale = 0;
            }
        }

        private void ConsumePlatform(AbstractPlatform platform)
        {
            m_PoolManager.Consume(platform);
        }
        private void LateUpdate()
        {
            if (isGamePlayStart)
            {
                if (mPlayerController.transform.position.y >=m_BorderController.mLastBorders.First().transform.position.y)
                    m_BorderController.BorderMover();

                if (mPlayerController.deathPosY - m_DeathPosOffset > mPlayerController.transform.position.y)
                {
                    m_CameraFollow.mCameraCanMove = false;
                    mPlayerController.mCollider2D.isTrigger = true;
                    m_InputManager.enabled = false;
                }

                else
                    m_CameraFollow.mCameraCanMove = true;
                
            }
        }
        private IEnumerator PeriodRunner(PeriodConfig defaultPeriod,bool inChallenge)
        {
            int periodId = 0;
            currentPeriodConfig=defaultPeriodConfig;
            while (inPeriod)
            {
                if (m_PlatformGenerator.CurrentPeriodConfig.PlatformsCount<=m_PlatformGenerator.GeneratedPlatformsCount &&!isLastPeriod&&!inChallenge)
                {
                    if (periodId == periods.Count - 1)
                    {
                        isLastPeriod = true;
                        yield return Yields.WaitForEndOfFrame;
                    }
                    currentPeriodConfig = periods[++periodId];
                    m_PlatformGenerator.CurrentPeriodConfig =currentPeriodConfig;
                    m_PlatformGenerator.GeneratedPlatformsCount = 0;
                }
                yield return Yields.WaitForEndOfFrame;
            }
        }
        private void PauseGamePlay(bool pause)
        {
            if (pause)
            {
                m_PlatformGenerator.GeneratorEnable = false;
                pausePopUp=  m_GameManager.mUiManager.PopUpController.Show<PausePopUp>(new PausePopUpData(), false);
                landedPlatform.IsPause = true;
                pausePopUp.OnResume += PauseGamePlay;
                pausePopUp.OnQuit += Quit;
                pausePopUp.OnRestart += Restart;

            }
            else
            {
                m_PlatformGenerator.GeneratorEnable = true;
                landedPlatform.IsPause = false;
                pausePopUp?.Close();
                pausePopUp.OnResume -= PauseGamePlay;
                pausePopUp.OnQuit -= Quit;
                pausePopUp.OnRestart -= Restart;
            }
        }
        public void Replay()
        {
            CleanUp();
            StartGamePlay();
        }
        private void Restart()
        {
            isGamePlayStart = false;
            inGamePlay = false;
            m_PlatformGenerator.GameOver();
            mPlayerController.GameOver();
            m_ScoreManager.GameOver(); 
            Replay();
            pausePopUp?.Close();
            pausePopUp.OnResume -= PauseGamePlay;
            pausePopUp.OnQuit -= GameOver;
            pausePopUp.OnRestart -= Restart;
            
        }
        public void GameOver()
        { 

            m_GameManager.mAudioManager.Play(SoundType.GameOverSound);
            m_PlatformGenerator.GameOver();
            mPlayerController.GameOver();
            m_ScoreManager.GameOver();
            m_SaveManager.SaveData.GamePlayTime = GetGamePlayTime(DateTime.Now); 
            gamePlayTime=DateTime.Now -startGamePlayTime;
            
            gameOverPopUp = m_GameManager.mUiManager.PopUpController.Show<GameOverPopUp>(new GameOverPopUpData()
            {
                coinsAmount = m_SaveManager.SaveData.CoinsAmount,
                currentCoins = m_SaveManager.SaveData.CurrentCoins,
                EconomyController = m_GameManager.EconomyController,
                ThemesController = m_GameManager.ThemesController,
                InChallenge = this.inChallenge

            }, true);
            if (gameOverPopUp!=null)
            {
                gameOverPopUp.OnReplay += Replay;
                gameOverPopUp.OnLeftGamePlay += OnMainView;
            }
            
            m_GameManager.EconomyController.Take(m_SaveManager.SaveData.CurrentCoins);
            m_SaveManager.SaveData.ScoreDashboard= SetInDashboard(m_ScoreManager.currentScore,gamePlayTime);

            mPlayerController.gameObject.SetActive(false);
            isGamePlayStart = false;
            inGamePlay = false;
            inPeriod = false;
            m_SaveManager.Serialize("game-data.json");

        }
        private void Quit()
        {
            m_GameManager.mAudioManager.Play(SoundType.GameOverSound);
            m_PlatformGenerator.GameOver();
            mPlayerController.GameOver();
            m_ScoreManager.GameOver();
           
            mPlayerController.gameObject.SetActive(false);
            isGamePlayStart = false;
            inGamePlay = false;
            inPeriod = false;
            CleanUp();

            m_GameManager.mUiManager.ViewController.Show<MainView>(new MainViewData()
            {
                coinsAmount = m_SaveManager.SaveData.CoinsAmount,
                EconomyController = m_GameManager.EconomyController,
                ThemesController = m_GameManager.ThemesController
            }, true);
            pausePopUp?.Close();
        }

        private void OnMainView()
        {
            CleanUp();
            gameOverPopUp.OnLeftGamePlay -= OnMainView;
            m_GameManager.mUiManager.mBackground.gameObject.SetActive(true);
            m_WorldBackground.gameObject.SetActive(false);

        }
        public void CleanUp()
        { 
            m_PlatformGenerator.CleanUp();
            m_BorderController.CleanUp();
            
            m_DeathZone.OnAddPool -= ConsumePlatform;
            mPlayerController.OnPlatformLended -= PlatformLanded;
            m_DeathZone.OnPlayerDeath -= GameOver;
            gamePlayView.OnPause -= PauseGamePlay;
            
            mPlayerController.gameObject.SetActive(false);
            // mPlayerController.OnPlatformFall -=PlatformFall;
        }

        private void FitBeckground(UnityEngine.Camera camera, SpriteRenderer backgroundSprite)
        {
            float spriteWidth = backgroundSprite.sprite.bounds.size.x;
            float spriteHeight = backgroundSprite.sprite.bounds.size.y;
     
            float worldScreenHeight = camera.orthographicSize * 2.0f;
            float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            Vector2 spriteScale =new Vector2(worldScreenWidth / spriteWidth,worldScreenHeight / spriteHeight);
            backgroundSprite.transform.localScale = spriteScale;
        }

        private List<int> SetInDashboard(int score, TimeSpan time)
        {
            scoreDashboard.Add(score);
            scoreDashboard.Sort();
            if (scoreDashboard.Count>m_RecordsCount)
                scoreDashboard.RemoveAt(0);
            return scoreDashboard;
        }
       
        private string GetGamePlayTime(DateTime time)
        { 
            string stringSec = "";
            int dTimeToSec =(int) (time-startGamePlayTime).TotalSeconds;
            int sec = dTimeToSec % 60;
            int min = (dTimeToSec-sec) / 60;
            if (sec < 10)
                stringSec = "0" + sec;
           
            else
                stringSec = sec.ToString();
            return min + ":" + stringSec;
        }
    }
}
