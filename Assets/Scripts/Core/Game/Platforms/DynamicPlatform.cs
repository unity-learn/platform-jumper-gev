﻿using System;
using System.Collections;
using DefaultNamespace;
using Services.Yielders;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Game.Platforms
{
    public class DynamicPlatform:AbstractPlatform
    {
        [SerializeField] private float m_DynemicPlatformMoveSpeed;
        
        private Vector2 topPos;
        private Vector2 bottomPos;
        
        private bool topMove;
        private float moveDistance;

        private void Awake()
        {
            XPosCamViewPort = Random.Range(0, 2);
            moveDistance = camera.orthographicSize /8;
            SpawnPosX = camera.ViewportToWorldPoint(new Vector3(Random.Range(0,2), 0, camera.nearClipPlane)).x;
        }

        private void OnEnable()
        {
           
            DynamicPlatformInMove = true;
            mRb.isKinematic = true;
            IsFall = false;
            mPlatformFall = false;
            topMove = false;
            inIndicating = true;
            startIndicating = true;
            StartCoroutine(Start());
        }

        private IEnumerator Start()
        {
            yield return Yields.WaitForEndOfFrame;
           StartPos = transform.position;
           topPos=new Vector2(StartPos.x,StartPos.y+moveDistance);
           bottomPos=new Vector2(StartPos.x,StartPos.y-moveDistance);
           StopCoroutine(Start());
        }

        private void Update()
        {
            if (!topMove&&DynamicPlatformInMove)
            {
                transform.position -=Time.deltaTime*m_DynemicPlatformMoveSpeed*transform.up;
               if (transform.position.y<=bottomPos.y)
                   topMove = !topMove; 

            } 
            else if (topMove&&DynamicPlatformInMove)
            {
                transform.position +=Time.deltaTime*m_DynemicPlatformMoveSpeed*transform.up;
              if (transform.position.y>=topPos.y)
                  topMove = !topMove; 
            }        
        }
    }
}