﻿using System;
using UnityEngine;

namespace Core.Game.Platforms
{
    public class LeftPlatform:AbstractPlatform
    {
        
        private void Awake()
        {
            mPlatformFall = false;
            SpawnPosX = camera.ViewportToWorldPoint(new Vector3(0f, 0, camera.nearClipPlane)).x;
        }

        
    }
}