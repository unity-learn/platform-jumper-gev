﻿using System;
using UnityEngine;

namespace Core.Game.Platforms
{
    public class RightPlatform:AbstractPlatform
    {
        
        private void Awake()
        {
            mPlatformFall = false;
            SpawnPosX =camera.ViewportToWorldPoint(new Vector3(1f, 0, camera.nearClipPlane)).x;
            
        }

    }
}