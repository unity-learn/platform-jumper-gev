﻿using System;
using System.Collections;
using Core.Mechanic.Pool;
using DefaultNamespace;
using Services.Yielders;
using UnityEngine;

namespace Core.Game.Platforms
{
    public abstract class AbstractPlatform : MonoBehaviour, IPoolObject
    {
        [SerializeField] protected float m_FallSpeed;
        [SerializeField] protected float m_PlatformFallTime;
        [SerializeField] private Rigidbody2D m_Rb;
        [SerializeField] private float xPosCamViewPort;
        [SerializeField] private int platformScore = 1;
        [SerializeField] private SpriteMask m_IndicatorMask;
        [SerializeField] private SpriteRenderer m_PlatformSprite;
        private Vector2 indicatorPos;
        private Vector2 startPos;

        public Vector2 StartPos { get => startPos; set => startPos = value; }
        public SpriteRenderer mPlatformSprite => m_PlatformSprite;

        private float indicatingDirection;
        private float spriteWidth;
        private float spawnPosX;

        private int platformId;
        private bool m_IsFall = false;
        private bool platformFall;
        private bool isFirst;
        protected bool inIndicating;
        protected bool startIndicating;
      
        protected UnityEngine.Camera camera => UnityEngine.Camera.main;

        public bool IsFirst { get => isFirst; set => isFirst = value; }

        public float mPlatformFallTime { get => m_PlatformFallTime; set => m_PlatformFallTime = value; }

        public int PlatformId { get => platformId; set => platformId = value; }
        public float XPosCamViewPort { get => xPosCamViewPort; set => xPosCamViewPort = value; }

        public float SpawnPosX { get => spawnPosX; set => spawnPosX = value; }

        public int PlatformScore { get => platformScore; }

        public Rigidbody2D mRb { get => m_Rb; set => m_Rb = value; }

        public bool IsFall { get => m_IsFall; set => m_IsFall = value; }

        private bool dynamicPlatformInMove;
        public bool DynamicPlatformInMove { get => dynamicPlatformInMove; set => dynamicPlatformInMove = value; }

        private bool isPause;

        public bool IsPause
        {
            get => isPause;
            set
            {
                isPause = value;
                if (isPause)
                    inIndicating = false;
                else
                {
                    inIndicating = true;
                    startIndicating = true;
                    StartCoroutine(PlatformFall());
                    mPlatformFall = false;
                }
            }
        }

        public bool mPlatformFall
        {
            get => platformFall;
            set
            {
                platformFall = value;
                if (platformFall)
                {
                    if (!IsFirst)
                    {
                        StartCoroutine(PlatformFall());
                        mPlatformFall = false;
                        return;
                    }

                    IsFirst = false;
                }
            }
        }

        private void Awake()
        {
            indicatorPos = m_IndicatorMask.transform.localPosition;
            spriteWidth = m_PlatformSprite.bounds.size.x;
        }

        private void OnEnable()
        {
            mRb.isKinematic = true;
            IsFall = false;
            mPlatformFall = false;
            inIndicating = true;
            startIndicating = true;
        }

        private void OnDisable()
        {
            m_IndicatorMask.transform.localPosition = indicatorPos;
        }

        private IEnumerator PlatformFall()
        {
            IsFall = false;
            if (startIndicating)
                StartCoroutine(PlatformIndicating());
            yield return Yields.GetSecondsDelay(mPlatformFallTime);
            if (!IsPause)
            {
                StopCoroutine(PlatformIndicating());
                if (!isFirst)
                {
                    mRb.isKinematic = false;
                    mRb.gravityScale = 0;
                    IsFall = true;
                    inIndicating = false;
                    startIndicating = true;
                }

                //First platform (not falling)
                StopCoroutine(PlatformFall());
            }
        }

        private IEnumerator PlatformIndicating()
        {
            startIndicating = false;
            if (transform.position.x < 0)
                indicatingDirection = -1;

            if (transform.position.x > 0)
            
                indicatingDirection = 1;

            while (inIndicating)
            {
                var maskPosition = m_IndicatorMask.transform.position;
                maskPosition.x += ((m_PlatformSprite.bounds.size.x * indicatingDirection) / 2) / m_PlatformFallTime * Time.deltaTime;
                m_IndicatorMask.transform.position = maskPosition;
                yield return new WaitForEndOfFrame();
            }
        }

        private void LateUpdate()
        {
            if (IsFall)
            {
                transform.position -= Time.deltaTime * m_FallSpeed * transform.up;
            }
        }

        public void Activate()
        {
            this.gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            this.gameObject.SetActive(false);
            StopAllCoroutines();
        }
    }
}