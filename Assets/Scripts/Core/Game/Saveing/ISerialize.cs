﻿namespace Core.Game.Saveing
{
    public interface ISerialize
    {
        void Serialize(string fileName);
    }
}