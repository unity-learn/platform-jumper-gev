﻿namespace Core.Game.Saveing
{
    public interface IPathConfig {
        string BasePath { get; }
        string GetFilePath(string fileName);
    }
}