﻿namespace Core.Game.Saveing
{
    public interface IDeserialize
    { 
        Save Deserialize(string fileName);
    }
}