﻿using System.IO;
using UnityEngine;

namespace Core.Game.Saveing
{
    public class PathConfig : IPathConfig {
        private readonly string folderName;
        public string BasePath => Path.Combine(Application.persistentDataPath, folderName);

        public PathConfig(string folderName) {
            this.folderName = folderName;
        }

        public string GetFilePath(string fileName) {
            return Path.Combine(BasePath, fileName);
        }
    }
}