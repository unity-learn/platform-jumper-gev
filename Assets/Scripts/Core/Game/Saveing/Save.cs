﻿using System;
using System.Collections.Generic;
using Configs.Periods;
using Configs.Theme;

namespace Core.Game.Saveing
{
    [System.Serializable]
    public class Save
    {
        public int CurrentScore;
        public int HighScore;
        public int CoinsAmount;
        public int CurrentCoins;
        public int GameId;
        public string GamePlayTime;
        public string ThemeId;
        public string DefaultThemeId = "1";
        public int PlayerId;
        public bool IsMusicMute;
        public bool IsEffectsMute;
        public DateTime challengeTime;
        public List<int> ScoreDashboard;
        public Dictionary<String,int> DashBoard;
        public List<string> UnLockedThemes;

        //public List<ScoreDashboard> ScoreDashboard=new List<ScoreDashboard>();
    }
}