﻿namespace Core.Game
{
    public interface IStartGame
    {
        void Start();
    }
}