﻿using System;
using System.Collections.Generic;
using Configs.Products;
using Configs.Theme;
using Core.Game.Saveing;
using Managers;
using Newtonsoft.Json;
using UI.PopUp;
using UI.PopUp.BuyPopUp;
using UI.PopUp.GetCoinsPopUp;
using UI.PopUp.PopUpData;
using UnityEngine;
using UnityEngine.Monetization;

namespace Core.Game.Themes
{
    public class ThemesController : MonoBehaviour
    {
        public event Action OnUpdateTheme;
        [SerializeField] private GameManager m_GameManager;

        public List<ThemeConfig> ThemesConfigs;
        public List<ProductConfig> Products;
        private ThemeConfig CurrentTheme;
        private string loadedThemeId;
        public ThemeConfig mCurrentTheme { get => CurrentTheme;}
        
        public void Setup(string id)
        {
            LoadTheme(id,true);
            //m_GameManager.LoadManager.OnThemeLoad += AddLoadedTheme;
        }

        private ThemeConfig GetTheme(string id)
        {
            for (int i = 0; i < ThemesConfigs.Count; i++)
            {
                if (ThemesConfigs[i].mId==id)
                {
                    return ThemesConfigs[i];
                }
            }
            return null;
        }

        private ProductConfig GetProductInfo(string id)
        {
            for (int i = 0; i < Products.Count; i++)
            {
                if (Products[i].ProductId == id)
                    return Products[i];
            }

            return null;
        }
        public void UnLockCheck(string id)
        {
            if (!m_GameManager.SaveManager.SaveData.UnLockedThemes.Contains(id)) 
                m_GameManager.mUiManager.PopUpController.Show<BuyPopUp>(new BuyPopUpData()
                {
                    takenThemeId = id
                }, false);
            else
            {
               LoadTheme(id,true);
            }
        }
        public async void LoadTheme(string id,bool isUnlock=false)
        {
            ProductConfig product = GetProductInfo(id);
            if (!isUnlock)
            {
                bool isEnoughCoins = m_GameManager.EconomyController.PurchaseTheme(product.ProductPrice);
                if (!isEnoughCoins)
                {
                    m_GameManager.mUiManager.PopUpController.Show<GetCoinsPopUp>(new GetCoinsPopUpData(),
                        m_GameManager.mUiManager);
                }
                else
                {
                   // await m_GameManager.LoadManager.Download(LoadFileType.AssetBundle,"AssetBundles/" + product.name.ToLower());
                    m_GameManager.SaveManager.SaveData.UnLockedThemes.Add(id);
                    OnUpdateTheme?.Invoke();
                    SetTheme(id);
                }
            }
            else
            {
               
                //await m_GameManager.LoadManager.Download(LoadFileType.AssetBundle,"AssetBundles/" + product.name.ToLower());
                SetTheme(id);
            }


        }

       private void SetTheme(string id)
        {
            ThemeConfig themeConfig = GetTheme(id);
            CurrentTheme = themeConfig;
            m_GameManager.MGamePlayController.Setup(themeConfig);
            m_GameManager.mUiManager.Setup(themeConfig);
            m_GameManager.SaveManager.SaveData.ThemeId = themeConfig.mId;
            
        }

       private void AddLoadedTheme(ThemeConfig themeConfig)
       {
           if (themeConfig!=null && !ThemesConfigs.Contains(themeConfig))
           {
               ThemesConfigs.Add(themeConfig);
               Debug.Log(themeConfig+ " theme loaded and added");
           }
           else
           {
               throw new Exception(themeConfig+"  is  null");
           }
       }
    }
}