﻿using Configs.Theme;
using Core.Game.Themes;

namespace Core.Game
{
    public interface IEndGame
    {
        void EndGame();
    }
}