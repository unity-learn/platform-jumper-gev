﻿using System;
using Core.Game.Input;
using Core.Game.Platforms;
using DefaultNamespace;
using Managers;
using UnityEngine;

namespace Core.Game.Player
{
    public class PlayerController : MonoBehaviour,IStartGamePlay
    {
        public event Action<AbstractPlatform> OnPlatformLended;
        public event Action<Collision2D> OnPlatformFall;
        
        [SerializeField] private Animator m_PlayerAnimator;
        [SerializeField] private Rigidbody2D m_Rigidbody;
        [SerializeField] private Collider2D m_Collider2D;
        [SerializeField] private SpriteRenderer m_PlayerSpriteRenderer;
        [SerializeField] private  float m_InitialAngle=50;
        
        private UnityEngine.Camera camera;
        private InputManager inputManager;
        private GamePlayController m_GamePlayController;
        private AbstractPlatform nextPlatform;
        private AbstractPlatform landedPlatform;
        private Vector3 targetPos;

        private float jumpWidth;
        private float jumpHeight;
        private float staticJumpHeight;
        private float staticJumpWidth;
        private float worldScreenHeight;
        private float worldScreenWidth;
        public float deathPosY;
        private bool isJump;
        private bool isLanded;
        public Collider2D mCollider2D { get => m_Collider2D; set => m_Collider2D = value; }
        public Rigidbody2D mRigidbody { get => m_Rigidbody; set => m_Rigidbody = value; }



        public void Setup(UnityEngine.Camera camera,InputManager inputManager,GamePlayController gamePlayController)
        {
            this.camera = camera;
            this.inputManager = inputManager;
            this.m_GamePlayController = gamePlayController;
            mCollider2D.isTrigger = false;
            worldScreenHeight = this.camera.orthographicSize * 2.0f;
            worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        }
        public void StartGamePlay()
        {
            deathPosY = -worldScreenHeight / 2; //bottom center
            inputManager.OnPress += InputControl;
            isLanded = true;
        } 


        private void Update()
        
        {
            isJump = m_Rigidbody.velocity.y > 0;
        }

        private void OnPlatformCheck(AbstractPlatform platform)
        {
           
            deathPosY = transform.position.y;
            for (int i = 0; i < m_GamePlayController.mPlatformGenerator.ActivePlatforms.Count; i++)
            {
                if (m_GamePlayController.mPlatformGenerator.ActivePlatforms[i].PlatformId==platform.PlatformId+1)
                {
                    nextPlatform = m_GamePlayController.mPlatformGenerator.ActivePlatforms[i];
                    break;
                }
            }
            if (platform != null)
            {
                landedPlatform = platform;
                if (platform is LeftPlatform || platform is RightPlatform)
                {
                    OnPlatformLended?.Invoke(platform); 
                    return;
                }
                if(platform is DynamicPlatform)
                {
                    platform.DynamicPlatformInMove = false;
                    OnPlatformLended?.Invoke(platform);
                }
            }
        }

        private void InputControl(PressData pressData) 
        {
            if (!isJump&&isLanded)
            {
                if (pressData == PressData.Right)
                {
                    if (transform.position.x >= 0)
                        Jump(PressData.Up);
                    else
                        Jump(PressData.Right);
                    return;
                }

                if (pressData == PressData.Left)
                {
                    if (transform.position.x < 0)
                        Jump(PressData.Up);
                    else
                        Jump(PressData.Left);
                    return;
                }
                return;
            }
        }
        private void Jump(PressData jumpDirection)
        {
            
            targetPos = nextPlatform.transform.position;
            if (jumpDirection == PressData.Left && nextPlatform.transform.position.x > 0)
            {
                m_Collider2D.isTrigger = true;
                inputManager.enabled=false;
            }

            if (jumpDirection == PressData.Right && nextPlatform.transform.position.x < 0)
            {
                m_Collider2D.isTrigger = true;
                inputManager.enabled=false;
            }

            float angleBetweenObjects = 0;
            m_GamePlayController.mGameManager.mAudioManager.Play(SoundType.Jump);
            if (jumpDirection == PressData.Left)
                targetPos.x = nextPlatform.transform.position.x + nextPlatform.mPlatformSprite.bounds.size.x / 8;
            if (jumpDirection == PressData.Right)
                targetPos.x = nextPlatform.transform.position.x - nextPlatform.mPlatformSprite.bounds.size.x / 8;
            if (jumpDirection == PressData.Up)
            {
                if (transform.position.x>=0)
                    targetPos.x = nextPlatform.transform.position.x - nextPlatform.mPlatformSprite.bounds.size.x / 8;
                else
                    targetPos.x = nextPlatform.transform.position.x + nextPlatform.mPlatformSprite.bounds.size.x / 8;
            }
            
            float gravity = Physics.gravity.magnitude;
            float angle = m_InitialAngle * Mathf.Deg2Rad;
 
            Vector3 planarTarget = new Vector3(targetPos.x, 0, targetPos.z);
            Vector3 planarPostion = new Vector3(transform.position.x, 0, transform.position.z);
 
            float distance = Vector3.Distance(planarTarget, planarPostion);
            float yOffset = transform.position.y - targetPos.y;
            float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));
 
            Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));
 
           if (jumpDirection==PressData.Left)
                angleBetweenObjects = -Vector3.Angle(Vector3.forward, planarTarget - planarPostion);

            if (jumpDirection==PressData.Right)
                angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPostion);
            Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;
            
            
            if (landedPlatform is DynamicPlatform && landedPlatform.StartPos.y >= transform.position.y - m_PlayerSpriteRenderer.bounds.size.y / 1.5f)
            {
                m_Rigidbody.velocity = finalVelocity/1.25f ;
                return;
            }
            m_Rigidbody.velocity = finalVelocity;
            
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            AbstractPlatform platform = other.transform.GetComponent<AbstractPlatform>();
            if (platform!=null)
            {
                isLanded = true;
                if (!isJump)
                {
                    m_Rigidbody.Sleep();
                    m_Rigidbody.velocity = Vector2.zero;
                    OnPlatformCheck(platform);
                }
            }
        }

       
        private void OnCollisionExit2D(Collision2D other)
        {
            isLanded = false;
            /*if (isJump)
            {
                jumpAccess = false;
                OnPlatformFall?.Invoke(other);
            }
            */
        }
        
        public void GameOver()
        {
            mCollider2D.isTrigger = false;
            inputManager.OnPress -= InputControl;
        }

    }
}