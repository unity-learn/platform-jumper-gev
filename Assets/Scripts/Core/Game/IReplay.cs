﻿namespace Core.Game
{
    public interface IReplay
    {
        void Replay();
    }
}