﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Game.Themes;
using Services.Screen;
using UnityEngine;

namespace Core.Game.Border
{
    public class Bordercontroller : MonoBehaviour,IStartGamePlay,IClean
    {
        [SerializeField] private UnityEngine.Camera m_Camera;
        [SerializeField] private GameObject m_Border;
        [SerializeField] private Transform m_BordersContainer;
        [SerializeField] private Sprite m_BorderSprite;


        private LinkedList<GameObject> stage0Borders;
        private LinkedList<GameObject> stage1Borders;
        private LinkedList<GameObject> stage2Borders;
        private LinkedList<LinkedList<GameObject>> borders;
        private LinkedList<GameObject> lastBorders;
        private Vector2 scale;

        public LinkedList<GameObject> mLastBorders => lastBorders;

        public void Setup(Sprite borderSprite)
        {
           
            m_Border.GetComponent<SpriteRenderer>().sprite = borderSprite;
            borders=new LinkedList<LinkedList<GameObject>>();
            stage0Borders=new LinkedList<GameObject>();
            stage1Borders=new LinkedList<GameObject>();
            stage2Borders=new LinkedList<GameObject>();
            
            float borderSpriteHeight = m_BorderSprite.bounds.size.y;
            scale = new Vector3(1,ScreenFit.mWorldScreenHeight/borderSpriteHeight,1);
        }
        public void StartGamePlay()
        {
            //Borders stage start from 0

            Vector2 stage0RightBorderPos = GetBorderVerticalPosition(0, BorderType.Right);
            Vector2 stage0LeftBorderPos = GetBorderVerticalPosition(0, BorderType.Left);
            Vector2 stage1RightBorderPos = GetBorderVerticalPosition(1, BorderType.Right);
            Vector2 stage1LeftBorderPos = GetBorderVerticalPosition(1, BorderType.Left);
            Vector2 stage2RightBorderPos = GetBorderVerticalPosition(2, BorderType.Right);
            Vector2 stage2LeftBorderPos = GetBorderVerticalPosition(2, BorderType.Left);
            
            if (stage0Borders.Count!=0)
            {
                BorderActivator(stage0Borders.FirstOrDefault().gameObject,stage0RightBorderPos);
                BorderActivator(stage0Borders.LastOrDefault().gameObject,stage0LeftBorderPos);
            }
            else
            {
                BorderCreator(stage0RightBorderPos,stage0Borders);
                BorderCreator(stage0LeftBorderPos,stage0Borders);
            }
            if (stage1Borders.Count!=0)
            {
                BorderActivator(stage1Borders.FirstOrDefault().gameObject,stage1RightBorderPos);
                BorderActivator(stage1Borders.LastOrDefault().gameObject,stage1LeftBorderPos);
            }
            else
            {
                BorderCreator(stage1RightBorderPos,stage1Borders);
                BorderCreator(stage1LeftBorderPos,stage1Borders);
            }

            if (stage2Borders.Count!=0)
            {
                  BorderActivator(stage2Borders.FirstOrDefault().gameObject,stage2RightBorderPos);
                  BorderActivator(stage2Borders.LastOrDefault().gameObject,stage2LeftBorderPos);
            }
            else
            {
                BorderCreator(stage2RightBorderPos,stage2Borders);
                BorderCreator(stage2LeftBorderPos,stage2Borders);
            }

            borders.AddFirst(stage0Borders);
            borders.AddFirst(stage1Borders);
            borders.AddFirst(stage2Borders);

            lastBorders = borders.FirstOrDefault();
        }

        private void BorderActivator(GameObject border,Vector2 position)
        {
            border.gameObject.SetActive(true);
            border.transform.position = position;
        }
        private void BorderCreator(Vector2 platformPos,LinkedList<GameObject> stageList)
        {
            GameObject border = Instantiate(m_Border, platformPos, m_Border.transform.rotation);
            border.transform.parent = m_BordersContainer;
            border.transform.localScale = scale;
            stageList.AddLast(border);
        }
        public void BorderMover()
        { 
            foreach (var border in borders.LastOrDefault())
            {
                border.transform.position=new Vector3(border.transform.position.x,border.transform.position.y+3*ScreenFit.mWorldScreenHeight);
            }

            lastBorders = borders.LastOrDefault();
            borders.RemoveLast();
            borders.AddFirst(lastBorders);
        }
        
        private Vector2 GetBorderVerticalPosition(int stageIndex,BorderType borderType)
        {
            if (borderType==BorderType.Left)
                return  new Vector2(-ScreenFit.mWorldScreenWidth/2, (stageIndex-1)*ScreenFit.mWorldScreenHeight);
            
            return new Vector2(ScreenFit.mWorldScreenWidth/2, (stageIndex-1)*ScreenFit.mWorldScreenHeight);
        }
     
        public void CleanUp()
        {
            foreach (var stageBorder in borders)
            {
                foreach (var border in stageBorder)
                {
                    border.gameObject.SetActive(false);
                }
            }
            borders.Clear();
        }

       
        

    }
}