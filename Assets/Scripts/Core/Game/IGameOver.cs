﻿namespace Core.Game
{
    public interface IGameOver
    {
        void GameOver();
    }
}