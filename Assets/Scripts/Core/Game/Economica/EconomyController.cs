﻿using System;
using Managers;
using UI.PopUp.BuyPopUp;
using UnityEngine;

namespace Core.Game.Economica
{
    public class EconomyController
    {
        public event Action<int> OnCoinsUpdate; 
        private GameManager gameManager;
        private SaveManager saveManager;
        private int coinsAmount;
        public EconomyController(GameManager gameManager,SaveManager saveManager)
        {
            this.coinsAmount = saveManager.SaveData.CoinsAmount;
            this.saveManager = saveManager;
            this.gameManager = gameManager;
        }
        private int Get(int coinsAmount)
        {
            this.coinsAmount = this.coinsAmount - coinsAmount;
            saveManager.SaveData.CoinsAmount = this.coinsAmount;
            OnCoinsUpdate?.Invoke(this.coinsAmount);
            return this.coinsAmount;
            
        }

        public void Take(int coinsAmount)
        {
            Debug.Log(coinsAmount);
            this.coinsAmount += coinsAmount;
            saveManager.SaveData.CoinsAmount = this.coinsAmount;
            OnCoinsUpdate?.Invoke(this.coinsAmount);
        }

        public bool PurchaseTheme(int price)
        {
            if (coinsAmount<price)
                return false;
            else
            {
                Get(price);
                return true;
            }
        }
    }
}