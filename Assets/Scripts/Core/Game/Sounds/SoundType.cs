﻿namespace DefaultNamespace
{
    public enum SoundType
    {
        BackgroundMusic=0,
        ButtonClick,
        GameOverSound,
        Jump
    }
}