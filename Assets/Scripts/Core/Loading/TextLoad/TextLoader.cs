﻿using System;
using System.Collections;
using UnityEngine;

namespace Core.Loading.TextLoad
{
    public class TextLoader<T>:ILoadText where T:class
    {
        public event Action<T> OnTextLoad;
        public MonoBehaviour rutine { get; set; }
        public string TextPath { get; set; }
        
        public void DownloadText(string textPath, MonoBehaviour rutine)
        {
            TextPath = textPath;
            this.rutine = rutine;
            this.rutine.StartCoroutine(DownloadText(TextPath));
        }
        private IEnumerator DownloadText(string url)
        {
            Debug.Log(url);
            WWW www = new WWW(url);
            yield return www;
            if (www.error == null)
                OnTextLoad?.Invoke(www.text as T);
            
            else 
                Debug.Log("WWW download had an error:" + www.error);
            
        }
    }
}