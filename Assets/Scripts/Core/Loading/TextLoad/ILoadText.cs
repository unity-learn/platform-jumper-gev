﻿using UnityEngine;

namespace Core.Loading.TextLoad
{
    public interface ILoadText
    {
        MonoBehaviour rutine { get; set; }
        string TextPath { get; set; }
    }
}