﻿namespace Core.Loading
{
    public enum LoadFileType
    {
        Audio=0,
        Text,
        Texture,
        AssetBundle
    }
}