﻿using System;
using System.Collections;
using UnityEngine;

namespace Core.Loading.BundlesLoad
{
    public class AssetBundleLoader <T>:ILoadAssetBundle where T : class
    {
        public event Action<T> OnBundleLoad;

        public MonoBehaviour rutine { get; set; }
        public string BundlePath { get; set; }
        
        public void DownloadBundle(string bundlePath,MonoBehaviour rutine)
        {
            BundlePath = bundlePath;
            this.rutine = rutine;
            rutine.StartCoroutine(DownloadAndCache(BundlePath));
        }
        private IEnumerator DownloadAndCache (string bundlePath)
        {
            while (!Caching.ready) yield return null;
            
            using (WWW www = WWW.LoadFromCacheOrDownload(bundlePath, 0))
            {
                yield return www;
                if (www.error != null) throw new Exception("WWW download had an error:" + www.error);
                
                AssetBundle bundle = www.assetBundle;
                AssetBundleRequest bundleAssets = bundle.LoadAllAssetsAsync(typeof(ScriptableObject));
                OnBundleLoad?.Invoke(bundleAssets.asset as T);
                
                bundle.Unload(false);
            }
        }
    }
}