﻿using UnityEngine;

namespace Core.Loading.BundlesLoad
{
    public interface ILoadAssetBundle
    {
        MonoBehaviour rutine { get; set; }
        string BundlePath { get; set; }
    }
}