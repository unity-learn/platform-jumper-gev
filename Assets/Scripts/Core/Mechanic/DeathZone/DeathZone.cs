﻿using System;
using Core.Game.Platforms;
using Core.Game.Player;
using UnityEngine;

namespace Core.Mechanic.DeathZone
{
    public class DeathZone : MonoBehaviour
    {
        public event Action<AbstractPlatform> OnAddPool;
        public event Action OnPlayerDeath;

        private void OnCollisionEnter2D(Collision2D other)
        {
            
            AbstractPlatform platform = other.gameObject.GetComponent<AbstractPlatform>();
            
            if (platform!=null)
            {
                platform.mRb.isKinematic = true;
                platform.IsFall = false;
                OnAddPool?.Invoke(platform); 
                return;
            }
            
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            PlayerController playerController = other.GetComponent<PlayerController>();
            if (playerController!=null)
            {
                OnPlayerDeath?.Invoke();
            }
        }
    }
}