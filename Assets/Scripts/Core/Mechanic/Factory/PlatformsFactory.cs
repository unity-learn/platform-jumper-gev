﻿using System.Collections.Generic;
using Core.Game.Platforms;
using Core.Mechanic.Pool;
using UnityEngine;

namespace Core.Mechanic.Factory
{
    public class PlatformsFactory:MonoBehaviour
    {
        [SerializeField] private List<AbstractPlatform> platforms;


        public void Setup(List<AbstractPlatform> platforms)
        {
            this.platforms = platforms;
        }
        public T Create<T>(Transform container) where T : class, IPoolObject
        {
            for (int i = 0; i < platforms.Count; i++)
            {
                if (typeof(T).Name==platforms[i].name)
                {
                    return Instantiate(platforms[i],container) as T;
                }
            }

            return null;
        }
    }
}