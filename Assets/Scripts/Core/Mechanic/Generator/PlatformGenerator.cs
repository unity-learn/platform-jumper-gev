﻿using System;
using System.Collections;
using System.Collections.Generic;
using Configs.Periods;
using Core.Game;
using Core.Game.Platforms;
using Core.Mechanic.Pool;
using DefaultNamespace;
using Managers;
using Services.Screen;
using Services.Yielders;
using UnityEngine;
using Object = System.Object;
using Random = UnityEngine.Random;
namespace Core.Mechanic.Generator
{
    public class PlatformGenerator:MonoBehaviour,IStartGamePlay,IClean,IGameOver
    {
        [SerializeField] private Camera m_Camera;
        [SerializeField] private PoolManager m_PoolManager;
        [SerializeField] private DeathZone.DeathZone m_DeathZone;
        [SerializeField] private Transform m_LPlatformContainers;
        [SerializeField] private Transform m_RPlatformContainers;
        [SerializeField] private Transform m_DPlatformContainers;
        [SerializeField] private int m_SidePlatformPercent = 70;
        [SerializeField] private int m_DynemicPlatformPercent = 30;
        [SerializeField] private int m_DisplayPlatformsAmount = 5;
        [SerializeField] private int m_PlatformsPerAmount = 5;
        [SerializeField] private float m_GenerateRateTime=1;
        
        private List<AbstractPlatform> activePlatforms;
        
        private PeriodConfig m_CurrentPeriodConfig;
        private float spawnPosY;
        private float platformsWorldDistance;
        private float platformsScreenYDistance;
        private float yPos=0;
        private int generatedPlatformsCount;
        private bool _generatorEnable;
        private bool generatorRun;
        private int generatedPlatformId = 1;
        public bool GeneratorEnable
        {
            get => _generatorEnable; 
            set
            {
                _generatorEnable = value;
                if (!generatorRun && value)
                {
                    StartCoroutine(GenerateRunner());
                    return;
                }

                if(generatorRun && value)
                    Debug.Log("Platform generator in running, and you can not run it");
            } 
        }
        
        public int GeneratedPlatformsCount { get => generatedPlatformsCount; set => generatedPlatformsCount = value; }

        public PeriodConfig CurrentPeriodConfig { get => m_CurrentPeriodConfig; set => m_CurrentPeriodConfig = value; }

        public List<AbstractPlatform> ActivePlatforms => activePlatforms;

        public void Setup()
        {
            
            CleanUp();
            platformsScreenYDistance = (float) 1 /  (m_DisplayPlatformsAmount-1);
            m_PoolManager.Setup();
            activePlatforms=new List<AbstractPlatform>();

        }
       
        public void StartGamePlay()
        {
            yPos = 0;
            spawnPosY = 0;
            platformsWorldDistance = 0;
            generatedPlatformsCount = 0;
            _generatorEnable = true;
            m_DeathZone.OnAddPool += Consume;
            
            for (int i = 0; i < m_PlatformsPerAmount; i++)
            {
                if (i==0)
                {
                    AbstractPlatform firstPlatform = GetPlatform(m_CurrentPeriodConfig,1);
                    firstPlatform.PlatformId =generatedPlatformId++;
                    activePlatforms.Add(firstPlatform);
                    firstPlatform.transform.position=m_Camera.ViewportToWorldPoint(new Vector3(firstPlatform.XPosCamViewPort,yPos,m_Camera.nearClipPlane));
                    yPos += platformsScreenYDistance;
                }
                AbstractPlatform platform = GetPlatform(m_CurrentPeriodConfig);
                platform.PlatformId =generatedPlatformId++;
                activePlatforms.Add(platform);
                platform.transform.position=m_Camera.ViewportToWorldPoint(new Vector3(platform.XPosCamViewPort,yPos,m_Camera.nearClipPlane));
                yPos += platformsScreenYDistance;
            }

            platformsWorldDistance = ScreenFit.mWorldScreenHeight/ (m_DisplayPlatformsAmount - 1);
            spawnPosY = ScreenFit.mWorldScreenHeight + platformsWorldDistance * (m_PlatformsPerAmount - m_DisplayPlatformsAmount );
            StartCoroutine( GenerateRunner());
        }



        private AbstractPlatform GetPlatform(PeriodConfig periodConfig,int index=0)
        {
            m_SidePlatformPercent = periodConfig.SidePlatformPercent;
            m_DynemicPlatformPercent = periodConfig.DynamicPlatformPercent;
            
            AbstractPlatform platform;
            int percent = 0;
            generatedPlatformsCount++;
            
            if (index == 1)
            {
                platform = m_PoolManager.GetObject<RightPlatform>(m_RPlatformContainers);
                platform.mPlatformFallTime = periodConfig.StaticPlatformsFallTime;
                platform.IsFirst = true;
                return platform;
            }

            percent = 100 - Random.Range(0, 101);
            if (percent <= m_SidePlatformPercent)
            {
                percent = Random.Range(0, 2);
                if (percent == 0)
                {
                    platform = m_PoolManager.GetObject<RightPlatform>(m_RPlatformContainers);
                    platform.mPlatformFallTime = periodConfig.StaticPlatformsFallTime;
                    return platform;
                }

                platform = m_PoolManager.GetObject<LeftPlatform>(m_LPlatformContainers);
                platform.mPlatformFallTime = periodConfig.StaticPlatformsFallTime;
                return platform;
            }

            if (percent > 100 - m_DynemicPlatformPercent)
            {
                platform = m_PoolManager.GetObject<DynamicPlatform>(m_DPlatformContainers);
                platform.mPlatformFallTime = periodConfig.DynamicPlatformsFallTime;
                return platform;
            }
            return null;
        }

        private void Consume(AbstractPlatform platform)
        {
            activePlatforms.Remove(platform);
            if (platform is LeftPlatform)
            {
                m_PoolManager.Consume(platform as LeftPlatform);
                return;
            }

            if (platform is RightPlatform)
            {
                m_PoolManager.Consume(platform as RightPlatform);
                return;
            }

            if (platform is DynamicPlatform)
            {
                m_PoolManager.Consume(platform as DynamicPlatform);
                return;
            }
        }

        private IEnumerator GenerateRunner()
        {
            generatorRun = true;
            while (_generatorEnable)
            {            
                yield return Yields.GetSecondsDelay(m_GenerateRateTime);
                AbstractPlatform platform = GetPlatform(m_CurrentPeriodConfig);
                platform.PlatformId=generatedPlatformId++;
                activePlatforms.Add(platform);
                platform.transform.position=new Vector3(platform.SpawnPosX,spawnPosY,-9.7f);
                platform.transform.rotation = Quaternion.identity;
                spawnPosY += platformsWorldDistance;

            }
            generatorRun = false;
        }
        
        
        public void CleanUp()
        {
            
            foreach (Transform child in m_LPlatformContainers)
            {
                if (child.gameObject.active)
                    Consume(child.GetComponent<AbstractPlatform>());
            }

            foreach (Transform child in m_RPlatformContainers)
            {
                if (child.gameObject.active)
                    Consume(child.GetComponent<AbstractPlatform>());

            }

            foreach (Transform child in m_DPlatformContainers)
            {
                if (child.gameObject.active)
                    Consume(child.GetComponent<AbstractPlatform>());

            }
            m_DeathZone.OnAddPool -= Consume;

        }

        
        public void GameOver()
        { 
            StopAllCoroutines();
            activePlatforms.Clear();
            generatedPlatformId = 0;
        }
        
        
    } 
}