﻿using System;
using System.Collections.Generic;
using Core.Game.Platforms;
using UnityEngine;

namespace Core.Mechanic.Pool
{
    public class PoolObject<T>where T:AbstractPlatform
    {
        private Func<T> func;
        private LinkedList<T> objects;
        private T lastItem;

        public PoolObject(Func<T> func)
        {
           objects=new LinkedList<T>();
           this.func =func;
        }
        public T GetObject()
        {
            if (objects.Count>0)
            {
                AbstractPlatform platform = objects.Last.Value;
                objects.RemoveLast();
                platform.gameObject.SetActive(true);
                return (T) platform;
            }

            return func?.Invoke();
        }

        public void Consume(T obj)
        {
            objects.AddLast(obj);

        }
    }
}