﻿namespace Core.Mechanic.Pool
{
    public interface IPoolObject
    {
        void Activate();
        void Deactivate();
    }
}