﻿using Core.Game.Ads;

namespace Analytic
{
    public interface IAnalytic
    {
        void AdsShowAnalytic(string serviceName, AdType type);
    }
}