﻿using System.Collections.Generic;
using Core.Game.Ads;
using UnityEngine;
using UnityEngine.Analytics;

namespace Analytic.UnityAnalytics
{
    public class UnityAnalytic:IAnalytic
    {
        public void AdsShowAnalytic(string serviceName, AdType type)
        {
            Analytics.CustomEvent("Advertisment", new Dictionary<string, object>()
            {
                {"Service", serviceName},
                {"type", type},
            });
        }
    }
}