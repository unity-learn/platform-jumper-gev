﻿using System;
using System.Collections.Generic;
using Core.Game.Ads;
using GoogleMobileAds.Api;
using UnityEngine;

namespace Ads.AdMobe
{
    public class AdMob : IAdvertisement
    {
        public event Action<int> OnUpdateCoins;
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3014895573678750/9559477122";
#elif UNITY_IPHONE 
            string adUnitId = "ca-app-pub-3014895573678750~1784449958";
#else
            string adUnitId = "unexpected_platform";
#endif
        private BannerView bannerView;
        private RewardedAd rewardedAd;
        private Dictionary<AdType,string> identifierIds;
        public string ServiceName { get; set; }
        public string rewardPlacementId { get; set; }
        public int rewardCoinsAmount{ get; set; }
        public bool testMode { get; set; }
        public bool isBannerLoaded{ get; private set; }
        public bool isRewardAdLoaded{ get; private set; }

        public AdMob(int rewardCoinsAmount,string rewardPlacementId,Dictionary<AdType,string> identifierIds,bool testMode)
        {
            this.rewardCoinsAmount = rewardCoinsAmount;
            this.rewardPlacementId = rewardPlacementId;
            this.identifierIds = identifierIds;
            this.testMode = testMode;
            ServiceName = this.GetType().ToString();
            MobileAds.Initialize(adUnitId);
        }
 

        public bool Show(AdType adType)
        {
            Debug.Log("in admob");
            if (adType==AdType.Banner)
            {
                RequestBanner();
                bannerView.Show();
                if (isBannerLoaded)
                    return true;
                else
                    return false;
            }
            if (adType==AdType.RewardVideo)
            {
                RewardVideo();
                rewardedAd.Show();
                if (isRewardAdLoaded) 
                    return true;
                else
                    return false;
            }
            return false;
        }

        public void HideBanner()
        {
            bannerView.Destroy();
            return;
        }

        private void RewardVideo()
        {
            this.rewardedAd = new RewardedAd(identifierIds[AdType.RewardVideo]);
            this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
            this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
            this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;
            AdRequest request = new AdRequest.Builder().Build();
            this.rewardedAd.LoadAd(request);
        }

        private void RequestBanner()
        {
            this.bannerView = new BannerView(identifierIds[AdType.Banner], AdSize.Banner, AdPosition.Bottom);
            this.bannerView.OnAdLoaded += this.HandleOnAdLoaded;
            AdRequest request = new AdRequest.Builder().Build();
            this.bannerView.LoadAd(request);
        }

        private void HandleRewardedAdLoaded(object sender, EventArgs args)
        {
            isRewardAdLoaded = true;
        }
        
        
        private void HandleRewardedAdClosed(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardedAdClosed event received");
        }

        private void HandleUserEarnedReward(object sender, Reward args)
        {
            string type = args.Type;
            double amount = args.Amount;
            Debug.Log(amount);
            OnUpdateCoins?.Invoke(rewardCoinsAmount);

        }


        private void HandleOnAdLoaded(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleAdLoaded event received");
            isBannerLoaded = true;
        }
        
    }
}