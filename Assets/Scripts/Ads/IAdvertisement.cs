﻿using System;

namespace Core.Game.Ads
{
    public interface IAdvertisement
    {
        event Action<int> OnUpdateCoins;
        
        string ServiceName { get; set; }
        string rewardPlacementId { get; set; }
        int rewardCoinsAmount{ get; set; }
        bool testMode{ get; set; }
        bool Show(AdType adType);
        void HideBanner();
    }
}