﻿using System;
using Core.Game.Ads;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Ads.UnityAds
{
    public class UnityAds :IAdvertisement,IUnityAdsListener
    {
        public event Action<int> OnUpdateCoins;

#if UNITY_ANDROID
        string gameId = "3606811";
#elif UNITY_IPHONE
            string gameId = "3606810";
#else
            string gameId = "unexpected_platform";
#endif
       
        public string ServiceName { get; set; }
        public string rewardPlacementId { get; set; }
        public int rewardCoinsAmount { get; set; }
        public bool testMode { get; set; }

        public UnityAds(int rewardCoinsAmount,string rewardPlacementId,bool testMode)
        {
            this.rewardCoinsAmount = rewardCoinsAmount;
            this.rewardPlacementId = rewardPlacementId;
            this.testMode = testMode;
            ServiceName = this.GetType().ToString();
            Advertisement.Initialize(gameId, testMode);
        }
       

        public bool Show(AdType adType)
        {
            if (Advertisement.IsReady())
            {
                Debug.Log("in unity ads");
                if (adType == AdType.Banner)
                {
                    Advertisement.Banner.Show();
                    return true;
                }

                if (adType == AdType.RewardVideo)
                {
                    Advertisement.Show();
                    return true;
                }
                
            }

            return false;

        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (showResult == ShowResult.Finished && placementId ==rewardPlacementId)
            {
                Debug.Log("Ad is successfully finished ");
                OnUpdateCoins?.Invoke(rewardCoinsAmount);

            }
            else if (showResult == ShowResult.Skipped)
            {
                Debug.Log("You skip the ad");
            }
            else if (showResult == ShowResult.Failed)
            {
                Debug.Log("error to show ad");
            }
        }
        public void HideBanner()
        {
            Advertisement.Banner.Hide();
        }

        public void OnUnityAdsReady(string placementId)
        {
            
        }

        public void OnUnityAdsDidError(string message)
        {
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            
        }

    }
}