﻿using System;
using System.Collections.Generic;
using Configs.Theme;
using Core.Game;
using Core.Game.Themes;
using UI.PopUp;
using UI.PopUp.PopUpData;
using UI.Presentor;
using UI.Tutorial;
using UI.Views;
using UI.Views.ViewData;
using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class UiManager : MonoBehaviour
    {
        [SerializeField] private GameManager m_GameManager;
        [SerializeField] private Canvas m_Canvas;
        [SerializeField] private Transform m_ViewsContainer;
        [SerializeField] private Transform m_PopUpsContainer;
        [SerializeField] private Image m_UIBackground;
        [SerializeField] private List<AbstractPresenter> m_AbstractViews;
        [SerializeField] private List<AbstractPresenter> m_AbstractPopUps;
        [SerializeField] private SaveManager m_SaveManager;
        [SerializeField] private Tutorial m_TutorialPrefab;
        [SerializeField] private Image m_LoadIndicator;
        [SerializeField] private GameObject m_ConnectionErrorImage;
        
        private bool isUseViews;
        private bool isUsePopUps;

        
        public Image LoadIndicator => m_LoadIndicator;

        public GameObject ConnectionErrorImage => m_ConnectionErrorImage;

        public ViewController ViewController { get; private set; }
        public PopUpController PopUpController { get; private set; }
        
        public Image mBackground { get => m_UIBackground; set => m_UIBackground = value; }
        public GameManager GameManager => m_GameManager;

        public MainView MainView { get; private set; }

       
        public void Setup(ThemeConfig themeConfig)
        {
            m_UIBackground.sprite = themeConfig.mWorldBackgroundSprite;
            m_UIBackground.gameObject.SetActive(true);
            if (!isUseViews)
            {
                ViewController=new ViewController(this,m_AbstractViews,m_ViewsContainer);
                isUseViews = true;
            }

            if (!isUsePopUps)
            {
                PopUpController=new PopUpController(this,m_AbstractPopUps,m_PopUpsContainer);
                isUsePopUps = true;
            }
        }

        public void StartGame()
        {
            MainView=ViewController.Show<MainView>(new MainViewData()
            {
                coinsAmount = m_SaveManager.SaveData.CoinsAmount,
                UiManager = this,
                EconomyController =GameManager.EconomyController,
                ThemesController =GameManager.ThemesController
            },false);
            MainView.OnGamePlayStart += GamePlayStart;
        }
        private void GamePlayStart()
        {
            if (m_SaveManager.SaveData.GameId==0)
                Instantiate(m_TutorialPrefab, m_Canvas.transform);
            m_UIBackground.gameObject.SetActive(false);
            m_GameManager.StartGamePlay();
        }

    }
}