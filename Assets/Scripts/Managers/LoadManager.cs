﻿using System;
using System.Threading.Tasks;
using Configs.Theme;
using Core.Loading;
using Core.Loading.BundlesLoad;
using Core.Loading.TextLoad;
using Firebase.Storage;
using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class LoadManager
    {
        public event Action<ThemeConfig> OnThemeLoad;
        public event Action<string> OnProductLoad;

        private AssetBundleLoader<ThemeConfig> assetBundleLoader;
        private TextLoader<string> textLoader;

        private MonoBehaviour rutine;
        private AssetBundleRequest assetBundleRequest;
        private Image loadingIndicator;
        private GameObject connectionErrorPrefab;
        private FirebaseStorage storage;
        private Uri uri;


        public LoadManager(MonoBehaviour rutine, Image loadIndicator, GameObject connectionErrorPrefab, Canvas canvas)
        {
            this.rutine = rutine;
            this.loadingIndicator = loadIndicator;
            this.connectionErrorPrefab = connectionErrorPrefab;
            storage = FirebaseStorage.DefaultInstance;

            assetBundleLoader = new AssetBundleLoader<ThemeConfig>();
            textLoader = new TextLoader<string>();
        }

        public async Task Download(LoadFileType fileType, string filePath)
        {
            await GetDownloadFileLink(filePath);

            if (fileType == LoadFileType.AssetBundle)
            {
                assetBundleLoader.OnBundleLoad += BundleLoaded;
                assetBundleLoader.DownloadBundle(uri.ToString(), rutine);
                return;
            }

            if (fileType == LoadFileType.Text)
            {
                textLoader.OnTextLoad += TextLoaded;
                textLoader.DownloadText(uri.ToString(), rutine);
                return;
            }
        }

        private async Task GetDownloadFileLink(string file_ref)
        {
            storage = FirebaseStorage.DefaultInstance;
            StorageReference path_reference = storage.GetReference(file_ref);
            await path_reference.GetDownloadUrlAsync().ContinueWith((Task<Uri> task) =>
            {
                if (task.IsFaulted || task.IsCanceled)
                    Debug.Log("faulted or canceled");

                else
                {
                    this.uri = task.Result;
                    Debug.Log(uri);
                }
            });
        }

        private void TextLoaded(string obj)
        {
            textLoader.OnTextLoad -= TextLoaded;
            OnProductLoad?.Invoke(obj);
        }

        private void BundleLoaded(ThemeConfig themeConfig)
        {
            assetBundleLoader.OnBundleLoad -= BundleLoaded;
            OnThemeLoad?.Invoke(themeConfig);
        }

    }
}