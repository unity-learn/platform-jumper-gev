﻿using System;
using System.Collections.Generic;
using Ads.AdMobe;
using Ads.UnityAds;
using Core.Game.Ads;
using Core.Game.Atributs;

namespace Managers
{
    public class AdsManager :IAdvertisement
    {
        public event Action<int> OnUpdateCoins;
        public event Action<string, AdType> OnAdShow;

        private GameManager gameManager;
        private List<IAdvertisement> ads;
        private Dictionary<AdType, string> identifierIds;
        public string ServiceName { get; set; }
        public string rewardPlacementId { get; set; }
        public int rewardCoinsAmount { get; set; }
        public bool testMode { get; set; }
        public AdsManager(GameManager gameManager,int rewardCoinsAmount,string rewardPlacementId,bool testMode)
        {
            this.gameManager = gameManager;
            this.rewardCoinsAmount = rewardCoinsAmount;
            this.rewardPlacementId = rewardPlacementId;
            this.testMode = testMode;
            identifierIds=new Dictionary<AdType, string>();

#if UNITY_ANDROID
            identifierIds.Add(AdType.Banner,"ca-app-pub-3014895573678750/9559477122");
            identifierIds.Add(AdType.RewardVideo,"ca-app-pub-3014895573678750/9527383617");
#elif UNITY_IPHONE
            identifierIds.Add(AdType.Banner,"ca-app-pub-3014895573678750/9910637398");
            identifierIds.Add(AdType.RewardVideo,"ca-app-pub-3014895573678750/4989372802");
#endif

        }
        public void Setup()
        {
            ads=new List<IAdvertisement>();
            ads.Add(new UnityAds(rewardCoinsAmount,rewardPlacementId,testMode));
            //ads.Add(new AdMob(rewardCoinsAmount,rewardPlacementId,identifierIds,testMode));
            
            for (int i = 0; i < ads.Count; i++)
            {
                ads[i].OnUpdateCoins += UpdateCoins;
            }
            
        }

        public bool Show(AdType adType)
        { 
            for (int i = 0; i < ads.Count; i++)
            {
                if (ads[i].Show(adType))
                {
                    OnAdShow?.Invoke(ads[i].ServiceName,adType);
                    return true;
                }
            }
            return false;
        }

        public void HideBanner()
        {
        }

        private void UpdateCoins(int plusCoinsAmount)
        {
            gameManager.EconomyController.Take(plusCoinsAmount);
        }
    }
}