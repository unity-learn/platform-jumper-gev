﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Configs.Periods;
using Configs.Products;
using Core.Game;
using Core.Game.Ads;
using Core.Game.Atributs;
using Core.Game.DailyChallenge;
using Core.Game.Economica;
using Core.Game.Products;
using Core.Game.Saveing;
using Core.Game.Themes;
using Core.Loading;
using DefaultNamespace;
using Newtonsoft.Json;
using Services.Screen;
using Unity.RemoteConfig;
using UnityEngine;
using UnityEngine.UI;
using AudioType = Configs.Audio.AudioType;

namespace Managers
{
    public class GameManager : MonoBehaviour,IStartGamePlay
    {
        public event Action OnEndGame;
        [SerializeField] private GamePlayController m_GamePlayController;
        [SerializeField] private ScoreManager m_ScoreManager;
        [SerializeField] private SaveManager m_SaveManager;
        [SerializeField] private AudioManager m_AudioManager;
        [SerializeField] private UiManager m_UiManager;
        [SerializeField] private ThemesController m_ThemesController;
        [SerializeField] private PeriodConfig m_DailyChallengePeriod;
        [SerializeField] private Canvas m_Canvas;
        [SerializeField] private Camera m_Camera;
        public UiManager mUiManager { get => m_UiManager; set => m_UiManager = value; }

        public GamePlayController MGamePlayController => m_GamePlayController;
        public AudioManager mAudioManager => m_AudioManager;
        public SaveManager SaveManager => m_SaveManager;
        public ScoreManager ScoreManager => m_ScoreManager;
        public ThemesController ThemesController => m_ThemesController;
        public AdsManager AdsManager { get; private set; } 
        public AnalyticManager AnalyticManager { get; private set; }
        public EconomyController EconomyController { get; private set; }
        public LoadManager LoadManager { get; private set; }
        public DailyChallenge DailyChallenge { get; private set; }
        public ScreenFit ScreenFit { get; private set; }

        private void Awake()
        {
            Setup();
        }
        private  void Setup()
        {
          
            m_SaveManager.Setup("Saving"); 
            ConfigManager.FetchCompleted += FetchConfig;
            ConfigManager.FetchConfigs(new UserAtributs(), new AppAtributs());
            
            EconomyController=new EconomyController(this,m_SaveManager);
            AdsManager=new AdsManager(this,20,"rewardedVideo",false); 
            AnalyticManager=new AnalyticManager(this);
            ScreenFit=new ScreenFit(m_Camera);
            
            
            //LoadManager = new LoadManager(this,m_UiManager.LoadIndicator,m_UiManager.ConnectionErrorImage,m_Canvas); 
            //LoadManager.OnProductLoad += LoadProducts;
            // await LoadManager.Download(LoadFileType.Text,"Files/Json/products.json");
            
            m_ThemesController.Setup(m_SaveManager.SaveData.ThemeId);
            m_GamePlayController.Setup(m_ThemesController.mCurrentTheme);
            m_UiManager.Setup(m_ThemesController.mCurrentTheme);
            AdsManager.Setup();
            m_AudioManager.Setup(this);
            m_UiManager.StartGame();

            m_AudioManager.Play(SoundType.BackgroundMusic);
            mAudioManager.Mute(AudioType.Music,m_SaveManager.SaveData.IsMusicMute);
            mAudioManager.Mute(AudioType.Effect,m_SaveManager.SaveData.IsEffectsMute);
            
           // LoadManager.Start(m_SaveManager.SaveData.ProductJsonLink);
           DailyChallenge=new DailyChallenge(m_DailyChallengePeriod,this,m_UiManager.MainView.ChallengeTimeText);
           DailyChallenge.Setup(SaveManager.SaveData.challengeTime);

            //AdsManager.Show(AdType.Banner);
            m_UiManager.MainView.OnDailyChallenge += OnDailyChallenge;

        }

       
        public void StartGamePlay()
        { 
            m_GamePlayController.StartGamePlay();
            m_ScoreManager.StartGamePlay();
        }

        private void FetchConfig(ConfigResponse response)
        {
            ConfigManager.FetchCompleted -= FetchConfig;
            Debug.Log(ConfigManager.appConfig.GetInt("m_StartCoinsAmount"));
        }
      /*  private  void LoadProducts(string jsonData)
        {
            LoadManager.OnProductLoad -= LoadProducts;
            Products products =  JsonConvert.DeserializeObject<Products>(jsonData);
            ThemesController.Products = products.ProductConfigs;
        }
      */
      private void OnDailyChallenge()
      {
          if (DailyChallenge.ChallengeIsActve)
          {
              SaveManager.SaveData.challengeTime=DateTime.Now;
              m_GamePlayController.StartGamePlay(DailyChallenge.ChallengeIsActve);
              m_ScoreManager.StartGamePlay();
              m_UiManager.mBackground.gameObject.SetActive(false);
              DailyChallenge.Setup(SaveManager.SaveData.challengeTime);
              return;
          }
          Debug.Log("Challenge isn't actve");
         
      }
        private void OnApplicationQuit()
        {
            if (m_SaveManager.SaveData!=null) 
                m_SaveManager.Serialize("game-data.json");
            OnEndGame?.Invoke();
        }

    }
}