﻿using System;
using Core.Game.Input;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Managers
{
    public class InputManager : MonoBehaviour,IPointerClickHandler
    {
        public event Action<PressData> OnPress;
        
        [SerializeField] private Canvas m_Canvas;
        
        private float screenHeight;
        private float screenWidth;
        private void Awake()
        {
           // var scaleFactor = m_Canvas.scaleFactor;
           // screenHeight = Screen.height + (1 - scaleFactor) * Screen.height;
           // screenWidth = Screen.width + (1 - scaleFactor) * Screen.width;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            PressSideDeciding(eventData);
        }

        private void PressSideDeciding(PointerEventData eventData)
        {
            if (eventData.position.x < Screen.width/2) //Left
                OnPress?.Invoke(PressData.Left);

            if (eventData.position.x >= Screen.width/ 2 ) //Right 
                OnPress?.Invoke(PressData.Right);
            
        }
    }
}