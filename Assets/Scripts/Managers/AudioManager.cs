﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Configs.Audio;
using Core.Game;
using DefaultNamespace;
using Services.Yielders;
using UnityEngine;
using UnityEngine.Audio;
using AudioType = Configs.Audio.AudioType;

namespace Managers
{
    [CreateAssetMenu(fileName = "Managers", menuName = "Managers/AudioManager", order = 0)]

    public class AudioManager : ScriptableObject
    {
        [SerializeField] private AudioMixer m_AudioMixer;
        [SerializeField] private SoundConfig[] m_SoundConfigs;


        private LinkedList<AudioSource> audioSources;
        private GameObject sourceHolder;
        private MonoBehaviour routineRunner;
        public SoundConfig[] SoundConfigs => m_SoundConfigs;
        
        public void Setup(MonoBehaviour rutineRunner)
        {
            this.routineRunner = rutineRunner;
            audioSources=new LinkedList<AudioSource>();
            sourceHolder=new GameObject("SoundHolder");
            DontDestroyOnLoad(sourceHolder);
        }

        public void Play(SoundType soundType) 
        {
            foreach (var config in m_SoundConfigs)
            {
                if (config.mSoundType==soundType)
                {
                    Play(config);
                    return;
                }
            }
        }
        private void Play(SoundConfig soundConfig)
        {
            AudioSource audioSource;
            if (soundConfig!=null)
            {
                audioSource = PlaySource(soundConfig);
                routineRunner.StartCoroutine(Player(audioSource)); 
                return;
            }

            m_SoundConfigs.FirstOrDefault();
            Debug.Log("can't find config or config is null"); 

        }
        
        private AudioSource PlaySource(SoundConfig soundConfig)
        {
            AudioSource source = GetSource();
            source.clip = soundConfig.mClip;
            source.outputAudioMixerGroup = soundConfig.mAudioMixer;
            source.volume = soundConfig.mVolume;
            source.loop = soundConfig.mLoop;
            source.Play();
            return source;

        }
        private void Consume(AudioSource audioSource)
        {
            audioSources.AddLast(audioSource);
        }

        private IEnumerator Player(AudioSource audioSource)
        {
           
            while (audioSource.isPlaying)
            {
                yield return Yields.WaitForEndOfFrame;
            }
            Consume(audioSource);
        }
        private AudioSource GetSource()
        {
            AudioSource audioSource;
            if (audioSources.Count>0)
            {
                audioSource = audioSources.Last.Value;
                audioSources.RemoveLast();
                return audioSource;
            }
            else
            {
                audioSource = sourceHolder.AddComponent<AudioSource>();
                return audioSource;
            }
        }
        
        private void MuteMusic(bool isMute)
        {
            if (isMute)
                m_AudioMixer.SetFloat("m_MusicVolume", -80);
            else 
                m_AudioMixer.SetFloat("m_MusicVolume", 0);
        }

        private void MuteEffects(bool isMute)
        {
            if (isMute)
                m_AudioMixer.SetFloat("m_EffectsVolume", -80);
            else 
                m_AudioMixer.SetFloat("m_EffectsVolume", 0);
        }

        private void MuteMaster(bool isMute)
        {
            if (isMute)
                m_AudioMixer.SetFloat("m_MasterVolume", -80);
            else 
                m_AudioMixer.SetFloat("m_MasterVolume", 0);
        }

        public void Mute(AudioType audioType, bool isMute)
        {
            routineRunner.StartCoroutine(Delay(audioType, isMute));
        }

        private IEnumerator Delay(AudioType audioType, bool isMute)
        {
            yield return Time.deltaTime;
            if (audioType == AudioType.Effect)
            {
                MuteEffects(isMute);
                routineRunner.StopCoroutine(Delay(audioType, isMute));
                yield break;
            }

            if (audioType==AudioType.Music)
            {
                MuteMusic(isMute);
                routineRunner.StopCoroutine(Delay(audioType, isMute));
                yield break;
            }

            if (audioType==AudioType.Master)
            {
                MuteMaster(isMute);
                routineRunner.StopCoroutine(Delay(audioType, isMute));
                yield break;
            }
        }
    }
}