﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Game;
using Core.Game.Saveing;
using UnityEngine;

namespace Managers
{
    [CreateAssetMenu(fileName = "Managers", menuName = "Managers/ScoreManager", order = 0)]

    public class ScoreManager:ScriptableObject,IStartGamePlay,IGameOver
    {
        public event Action<int, int> OnScoreUpdate;

        [SerializeField] private SaveManager m_SaveManager;
        public int currentScore { get; private set; }
        public int highScore{ get; private set; }
        public int coinsAmount{ get; private set; }
        public int currentCoins{ get; private set; }

        public void StartGamePlay()
        {
            currentScore = 0;
            currentCoins = 0;
            highScore = m_SaveManager.SaveData.HighScore;
            coinsAmount = m_SaveManager.SaveData.CoinsAmount;
            OnScoreUpdate?.Invoke(highScore, currentScore);
                
        }
        public void UpdateScore(int pluseScore=0)
        {
            currentScore += pluseScore;
            if (currentScore>=highScore)
                highScore = currentScore;
            OnScoreUpdate?.Invoke(highScore,currentScore);
        }
        public void GameOver()
        {
            currentCoins=CoinsCalculator(currentScore);
            m_SaveManager.SaveData.CoinsAmount += currentCoins;
            m_SaveManager.SaveData.CurrentCoins = currentCoins;
            m_SaveManager.SaveData.CurrentScore = currentScore;
            m_SaveManager.SaveData.HighScore = highScore;
            
        }
        private int CoinsCalculator(int score)
        {
            currentCoins= (int) (score*4);
            return currentCoins;
        }
        
        
    }
}