﻿using System.Collections.Generic;
using Analytic;
using Analytic.UnityAnalytics;
using Core.Game.Ads;

namespace Managers
{
    public class AnalyticManager:IAnalytic
    {
        private List<IAnalytic> analyticServices;
        private GameManager gameManager;
        

        public AnalyticManager(GameManager gameManager)
        {
            analyticServices=new List<IAnalytic>();
            analyticServices.Add(new UnityAnalytic());
            
            this.gameManager = gameManager;
            this.gameManager.AdsManager.OnAdShow+=AdsShowAnalytic;
        }

        public void AdsShowAnalytic(string serviceName, AdType type)
        {
            for (int i = 0; i < analyticServices.Count; i++)
            {
                analyticServices[i].AdsShowAnalytic(serviceName,type);
            }
        }
    }
}