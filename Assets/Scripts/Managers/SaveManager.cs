﻿using System;
using System.Collections.Generic;
using System.IO;
using Core.Game.Saveing;
using Newtonsoft.Json;
using UnityEngine;

namespace Managers
{
    [CreateAssetMenu(fileName = "Managers", menuName = "Managers/SaveManager", order = 0)]
    public class SaveManager : ScriptableObject,ISerialize,IDeserialize
    {
         private PathConfig pathConfig;
         private string filePath;
         private string folderName;

        public Save SaveData { get;private set; }

        public void Setup(string folderName)
        {
            pathConfig=new PathConfig(folderName);
            SaveData = Deserialize(pathConfig.GetFilePath("game-data.json"));
        }
        public void Serialize(string fileName)
        {
            filePath = pathConfig.GetFilePath(fileName);
            string jsonData = JsonConvert.SerializeObject(SaveData,Formatting.Indented);
            WriteContent(filePath, jsonData);

        }
        
        public Save Deserialize(string fileName)
        {
            filePath = pathConfig.GetFilePath(fileName);
            if (File.Exists(filePath))
            {
                string jsonData = ReadContent(filePath);
                Save content = JsonConvert.DeserializeObject<Save>(jsonData);                 
                return content;
            }
            else
            {
                Debug.Log("File is not exist");
                Directory.CreateDirectory(pathConfig.BasePath);
                Save data =new Save();
                data.ScoreDashboard=new List<int>();
                data.DashBoard=new Dictionary<string,int>();
                data.UnLockedThemes=new List<string>();
                data.UnLockedThemes.Add(data.DefaultThemeId);
                data.ThemeId = data.DefaultThemeId;
                data.challengeTime=DateTime.Now;//Default theme id
                return data;
            }

        }

        private void WriteContent(string path, string content)
        {
            using ( StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(content);
                sw.Close();
            }
        }    

        private string ReadContent(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                string content = sr.ReadToEnd();
                sr.Close();
                return content;
            }
            
        }

    }
}