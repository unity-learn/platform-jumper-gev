﻿using System;
using System.Collections.Generic;
using Core.Game;
using Core.Game.Platforms;
using Core.Mechanic.DeathZone;
using Core.Mechanic.Factory;
using Core.Mechanic.Pool;
using DefaultNamespace;
using UnityEngine;

namespace Managers
{
    public class PoolManager : MonoBehaviour
    {
        [SerializeField] private PlatformsFactory m_PlatformsFactory;
        [SerializeField] private DeathZone m_DeathZone;

        private Dictionary<Type, Queue<IPoolObject>> pool;

        public void Setup()
        {
            pool = new Dictionary<Type, Queue<IPoolObject>>();
        }
        public T GetObject<T>(Transform container) where T : class, IPoolObject
        {
            if (pool.ContainsKey(typeof(T)))
            {
                if (pool[typeof(T)].Count != 0)
                {
                    var poolObject = pool[typeof(T)].Dequeue();
                    poolObject.Activate();
                    return poolObject as T;
                }
                else
                {
                    var poolObject = m_PlatformsFactory.Create<T>(container);
                    poolObject.Activate();
                    return poolObject;
                }
            }
            else
            {
                var poolObject = m_PlatformsFactory.Create<T>(container);
                poolObject.Activate();
                return poolObject;
            }
        }

        public void Consume<T>(T poolObject) where T : class, IPoolObject
        {
            poolObject.Deactivate();
            if (pool.ContainsKey(typeof(T)))
                pool[typeof(T)].Enqueue(poolObject);

            else
                pool.Add(typeof(T), new Queue<IPoolObject>());
        }

        private void InDeathZone(AbstractPlatform platform)
        {
            if (platform is LeftPlatform)
            {
                Consume(platform as LeftPlatform);
                return;
            }

            if (platform is RightPlatform)
            {
                Consume(platform as RightPlatform);
                return;
            }

            if (platform is DynamicPlatform)
            {
                Consume(platform as DynamicPlatform);
                return;
            }
        }
    }
}